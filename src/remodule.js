import remodule from './remodule.1';
import * as app from './remodule/redux-app';
import * as forms from './remodule/redux-forms';
import * as graphql from './remodule/redux-graphql';
import * as persisted from './remodule/redux-persisted';
import * as operations from './remodule/redux-operations';
import * as persistedCollection from './remodule/redux-persisted-collection';
import * as persistedHistory from './remodule/redux-persisted-history';
import * as request from './remodule/redux-request';
import * as requestCollection from './remodule/redux-request-collection';
import * as requestHistory from './remodule/redux-request-history';
import * as schemas from './remodule/redux-schemas';
import * as settings from './remodule/redux-settings';
import * as navigation from './remodule/redux-navigation';
import * as projects from './remodule/redux-projects';

export default remodule(
  [
    app,
    forms,
    graphql,
    navigation,
    operations,
    persisted,
    persistedCollection,
    persistedHistory,
    projects,
    request,
    requestCollection,
    requestHistory,
    schemas,
    settings
  ],
  { initial: true }
);
