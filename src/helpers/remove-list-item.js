export default function removeListItem(list, id) {
  const index = list.indexOf(id);

  if (index < 0) {
    return list;
  } else {
    return [...list.slice(0, index), ...list.slice(index + 1)];
  }
}
