export default function objectToArray({ obj, fn }) {
  return Object.keys(obj).reduce((prev, curr) => {
    return [...prev, fn(curr)];
  }, []);
}
