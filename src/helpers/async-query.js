import axios from 'axios';
import cleanObj from './clean-object';

// make ajax request to graphql server
export function query(args) {
  const { actions, auth, query, url, variables, filter, persisted } = args;

  axios.defaults.baseURL = url;
  axios.defaults.headers.common['Authorization'] = JSON.stringify(auth);
  axios.defaults.headers.post['Content-Type'] =
    'application/x-www-form-urlencoded';

  const data = {
    actions,
    filter,
    query: `${query}`,
    variables,
    persisted
  };

  const axiosConfig = {
    url,
    dataType: 'jsonp',
    method: 'POST'
  };

  return axios({ ...axiosConfig, data }).then(response => {
    // handle errors
    if (response.data.message && response.data.message.error) {
      return { error: response.data.message };
    }

    // handle successful response
    const data = actions.reduce((prev, curr) => {
      return {
        ...prev,
        [curr]:
          response.data.data[curr] != null
            ? cleanObj(response.data.data[curr])
            : null
      };
    }, {});

    return { data };
  });
}

export function makeQuery({ args, fields, operation, type }) {
  let operationArgs = args.reduce(
    (previous, arg) => {
      const variableTypes = `$${arg.name}: ${arg.kind === 'ListType'
        ? '[' + arg.type + ']'
        : arg.type}`;
      const params = `${arg.name}: $${arg.name}`;

      return {
        types: previous.types
          ? previous.types + '\n' + variableTypes
          : variableTypes,
        params: previous.params ? previous.params + '\n' + params : params
      };
    },
    { types: '', params: '' }
  );

  if (operationArgs.types.trim() !== '') {
    operationArgs.types = `(
      ${operationArgs.types}
    )`;
  }

  if (operationArgs.params.trim() !== '') {
    operationArgs.params = `(
      ${operationArgs.params}
    )`;
  }

  const data = `${type}
  ${operation}
  ${operationArgs.types}
  {
    ${operation} ${operationArgs.params}
      {
    ${fields.reduce(
      (previous, field) =>
        previous ? previous + '\n' + field.name : field.name,
      ''
    )}
   }
  }`;

  return data;
}
