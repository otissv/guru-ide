import React from 'react';
import autobind from 'class-autobind';
import { buildClientSchema } from 'graphql';
import getClassMethods from '../../../helpers/get-class-methods';
import { connect } from '../../../store';

class LayoutContainer extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);
  }

  componentWillMount() {
    const {
      project: { endpoint },
      getGraphqlSchema,
      setGraphql,
      setSchemaIsConnected
    } = this.props;

    getGraphqlSchema(endpoint)
      .payload.then(response => {
        if (response.data && response.data.__schema) {
          setGraphql({
            introspection: response.data,
            builtSchema: buildClientSchema(response.data)
          });
          setSchemaIsConnected(true);
        }
      })
      .catch(error => {
        console.error(error);
        setGraphql({
          introspection: {},
          builtSchema: {}
        });
        setSchemaIsConnected(false);
      });
  }

  render() {
    const Component = this.props.component;

    return <Component {...getClassMethods(this)} />;
  }
}

export default connect(LayoutContainer);
