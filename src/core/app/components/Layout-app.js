import React, { PureComponent } from 'react';
import { renderRoutes } from 'react-router-config';
import Nav from '../../../modules/navigation/components/Navigation';
import styled from 'styled-components';
import Statusbar from '../../../modules/statusbar/components/Statusbar';
import Project from '../../../modules/projects/components/Projects';

import routes from '../../../modules/router/routes';

const LayoutContainer = styled.div`display: flex;`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Main = styled.div`
  display: flex;
  flex: 1;
`;

const Page = styled.div`
  bottom: 0;
  display: flex;
  flex-direction: column;
  flex: 1;
  left: 0;
  right: 0;
`;

const TitleBar = styled.div`
  border-bottom: ${props => props.theme.border.thinPrimary};
  z-index: 1;
  position: relative;
  width: 100%;
  padding: 2px 0;
`;

const Title = styled.h1`
  font-size: ${props => props.theme.font.large};
  letter-spacing: 1px;
  margin: 0 0 0 20px;
`;

export default class Layout extends PureComponent {
  componentWillMount() {
    const { setTitle } = this.props;

    const title = routes[0].routes
      .filter(route => route.active === true)[0]
      .label.toLocaleLowerCase();

    setTitle(title);
  }

  render() {
    const { project, route, title } = this.props;
    const showProject = project.name.trim() === '';

    return (
      <LayoutContainer className="Layout-container">
        {!showProject && <Nav {...this.props} />}
        {showProject ? (
          <Project />
        ) : (
          <Content className="Layout-content">
            <TitleBar className="Layout-title-bar">
              <Title className="Layout-title">{`${title[0].toUpperCase()}${title.substr(
                1,
                title.length - 1
              )}`}</Title>
            </TitleBar>
            <Main className="Layout-main">
              <Page className="Layout-page">{renderRoutes(route.routes)}</Page>
            </Main>
          </Content>
        )}
        {!showProject && <Statusbar className="Layout-status-bar" />}
      </LayoutContainer>
    );
  }
}
