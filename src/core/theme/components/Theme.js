import React, { PureComponent } from 'react';
import styled from 'styled-components';
import theme from '../../../styled/themes/theme';
import { ThemeProvider } from 'styled-components';

export default class Theme extends PureComponent {
  render() {
    return <ThemeProvider theme={theme()}>{this.props.children}</ThemeProvider>;
  }
}
