import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Svg from './Svg';
import closeIcon from '../../icons/cross.svg';

const LogoContainer = styled.div`
  border-bottom: ${props => props.theme.logo.borderBottom};
  padding: ${props => props.theme.logo.padding};
  width: ${props => props.theme.logo.width};
`;

const LogoImg = styled.img`
  color: ${props => props.theme.logo.img.color};
  display: ${props => props.theme.logo.img.display};
  font-size: ${props => props.theme.logo.img.fontSize};
  height: ${props => props.theme.logo.img.height};
  text-decoration: ${props => props.theme.logo.img.textDecoration};
`;

export default class Logo extends PureComponent {
  render() {
    return (
      <LogoContainer>
        <LogoImg className="Logo" {...this.props} />
      </LogoContainer>
    );
  }
}
