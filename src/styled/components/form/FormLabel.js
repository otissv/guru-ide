import React, { PureComponent } from 'react';
import styled from 'styled-components';

const FormLabelStyled = styled.label`
  display: ${props => props.display || props.theme.form.label.display};
  margin-bottom: ${props =>
    props.display || props.theme.form.label.marginBottom};
  font-weight: ${props => props.display || props.theme.form.label.fontWeight};
  ${props => props.styledFormLabel};
`;

export default class FormLabel extends PureComponent {
  render() {
    return <FormLabelStyled className="Form-label" {...this.props} />;
  }
}
