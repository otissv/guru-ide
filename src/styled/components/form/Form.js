import React, { PureComponent } from 'react';
import styled from 'styled-components';

const FormStyled = styled.form`${props => props.styledForm};`;

export default class Form extends PureComponent {
  render() {
    return <FormStyled className="Form" {...this.props} />;
  }
}
