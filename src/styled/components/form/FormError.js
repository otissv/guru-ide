import React, { PureComponent } from 'react';
import styled from 'styled-components';

const FormErrorStyled = styled.p`
  display: ${props => props.display || props.theme.form.error.display};
  color: ${props => props.theme.form.error.color};
  background: ${props => props.theme.form.error.background};
  margin: ${props => props.theme.form.error.margin};
  padding: ${props => props.theme.form.error.padding};

  ${props => props.styledFormError};
`;

export default class FormError extends PureComponent {
  render() {
    return <FormErrorStyled className="Form-error" {...this.props} />;
  }
}
