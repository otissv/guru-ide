import React, { PureComponent } from 'react';
import styled from 'styled-components';

const FormRowStyled = styled.div`
  margin-bottom: ${props => props.theme.form.row.marginBottom};
  ${props => props.styledFormRow};
`;

export default class FormRow extends PureComponent {
  render() {
    return <FormRowStyled className="Form-row" {...this.props} />;
  }
}
