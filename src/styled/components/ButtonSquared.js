import React, { PureComponent } from 'react';
import styled from 'styled-components';

const ButtonSquaredStyled = styled.button`
  background: ${props => props.theme.buttonSquared.background};
  border: ${props => props.theme.buttonSquared.border};
  color: ${props => props.theme.buttonSquared.color};
  cursor: ${props => props.theme.buttonSquared.cursor};
  height: ${props => props.theme.buttonSquared.height};
  line-height: ${props => props.theme.buttonSquared.lineHeight};
  margin: ${props => props.theme.buttonSquared.margin};
  outline: ${props => props.theme.buttonSquared.outline};
  text-align: ${props => props.theme.buttonSquared.textAlign};
  transition: ${props => props.theme.buttonSquared};
  width: ${props => props.theme.buttonSquared.width};

  &:hover {
    border: ${props => props.theme.buttonSquared.border};
    background: ${props => props.theme.buttonSquared.hover.background};
    color: ${props => props.theme.buttonSquared.color};
  }

  &:active {
    border: ${props => props.theme.buttonSquared.border};
    background: ${props => props.theme.buttonSquared.active.background};
    color: ${props => props.theme.buttonSquared.color};
  }

  &:focus {
    border: ${props => props.theme.buttonSquared.border};
    background: ${props => props.theme.buttonSquared.focus.background};
    color: ${props => props.theme.buttonSquared.color};
  }

  ${props => props.styledButtonSquared};
`;

export default class ButtonSquared extends PureComponent {
  render() {
    return <ButtonSquaredStyled className="Button-Circle" {...this.props} />;
  }
}
