export default function select({ animation, border, colors, font, spacing }) {
  return {
    position: 'relative',
    width: 'auto',

    options: {
      position: 'absolute',
      width: 'auto',
      maxHeight: '200px',
      border: border.thin,
      borderTop: 'none',
      width: '100%',
      background: colors.foreground,
      left: 0,

      item: {
        height: '30px',
        width: '100%',
        textAlign: 'left',
        padding: `0 ${spacing.small}`,
        background: 'none',
        color: colors.background,
        textTransform: 'none',
        hover: {
          background: 'none',
          color: colors.background
        }
      }
    },
    icon: {
      position: 'absolute',
      top: '1px',
      padding: spacing.xsmall,
      height: '25px',
      right: '1px',
      transition: `background ${animation.easeMedium}`,
      width: '27px',
      hover: {
        background: colors.secondary
      }
    }
  };
}
