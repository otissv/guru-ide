export default function close() {
  return {
    float: 'right',
    border: 'none',
    background: 'none',
    outline: 'none'
  };
}
