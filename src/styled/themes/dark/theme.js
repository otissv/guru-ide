import Accordion from './accordion-theme';
import Border from './border-theme';
import Button from './button-theme';
import ButtonSquared from './button-squared-theme';
import Checkbox from './checkbox-theme';
import Clearfix from './clearfix-theme';
import Close from './close-theme';
import Colors from './colors-theme';
import Font from './font-theme';
import Form from './form-theme';
import Global from './global-theme';
import Input from './input-theme';
import List from './list-theme';
import Logo from './logo-theme';
import Media from './media-theme';
import Modal from './modal-theme';
import Nav from './nav-theme';
import Scrollbar from './scrollbar-theme';
import Select from './select-theme';
import Sidebar from './sidebar-theme';
import Spacing from './spacing-theme';
import Statusbar from './statusbar-theme';
import Svg from './svg-theme';
import Tabs from './tabs-theme';
import Textarea from './textarea-theme';
import Toolbar from './toolbar-theme';

// base
export const colors = Colors();
export const border = Border({ colors });
export const clearfix = Clearfix();
export const font = Font({ colors });
export const media = Media();
export const spacing = Spacing();
export const scrollbar = Scrollbar({ colors, spacing });
export const animation = {
  slow: '0.5s',
  medium: '0.3s',
  fast: '0.1s',
  easeInOutSlow: '0.5s ease-in-out',
  easeInOutMedium: '0.3s ease-in-out',
  easeInOutFast: '0.1s ease-in-out'
};

const base = {
  animation,
  border,
  clearfix,
  colors,
  font,
  media,
  scrollbar,
  spacing
};

export const global = Global(base);

// modules
export const accordion = Accordion(base);
export const button = Button(base);
export const buttonSquared = ButtonSquared(base);
export const checkbox = Checkbox(base);
export const close = Close(base);
export const form = Form(base);
export const input = Input(base);
export const list = List(base);
export const logo = Logo(base);
export const modal = Modal(base);
export const nav = Nav(base);
export const select = Select(base);
export const sidebar = Sidebar(base);
export const statusbar = Statusbar(base);
export const svg = Svg(base);
export const tabs = Tabs(base);
export const textarea = Textarea(base);
export const toolbar = Toolbar(base);

export default {
  accordion,
  animation,
  border,
  button,
  buttonSquared,
  checkbox,
  clearfix,
  close,
  colors,
  font,
  form,
  input,
  list,
  logo,
  media,
  modal,
  nav,
  scrollbar,
  select,
  sidebar,
  spacing,
  statusbar,
  svg,
  tabs,
  textarea,
  toolbar
};
