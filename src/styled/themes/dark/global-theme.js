import { injectGlobal } from 'styled-components';
import normalizeStyled from 'styled-normalize';
import scrollbarTheme from './scrollbar-theme';

export default function global({ colors, font, spacing }) {
  const scrollbar = scrollbarTheme({ colors, spacing });

  return injectGlobal`
    ${normalizeStyled}
  
    html {
      line-height: ${font.lineHeight}
    }
    
    html,
    body,
    pre {
      background: ${colors.background};
      color: ${colors.foreground};
      letter-spacing: 1px !important;
    }

    body {
      padding: 0;
      background: ${colors.background};
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    a,
    button {
      font-family: ${font.serif};
      ${font.smooth};
      font-weight: normal;
      letter-spacing: ${font.letterSpacing};
    }

    body,
    p,
    a,
    button {
      font-size: ${font.default};
    }
    
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-size: ${font.large};
    }

    * {
      box-sizing: border-box;
    }

    ::selection {
      background: ${colors.primary};
    }

    pre {
      font-size: ${font.small};
      font-family: ${font.code};
      line-height: 1.3;
    }

    a, area, button, input, label, select, summary, textarea {
      touch-action: manipulation;
    }

    *::-webkit-scrollbar {
        width: ${scrollbar.width};
        height: ${scrollbar.height};
    }
    *::-webkit-scrollbar-corner {
      background-color: ${scrollbar.corner.backgroundColor};

    }
   
    *::-webkit-scrollbar-thumb {
      background-color: ${scrollbar.thumb.backgroundColor};
      outline:  ${scrollbar.thumb.outline.none};
    }
    
    *::-webkit-scrollbar-thumb:hover {
      background-color: ${scrollbar.thumb.hover.backgroundColor};
    }
  `;
}
