import { generateMedia } from 'styled-media-query';

export default function media() {
  return generateMedia({
    mobileS: '320px',
    mobileM: '375px',
    mobileL: '425px',
    tablet: '758px',
    laptop: '1024px',
    laptopM: '1400px',
    laptopL: '1920px',
    '4k': '2560px'
  });
}
