export default function form({ spacing }) {
  return {
    error: {
      display: 'none',
      color: '#ffffff',
      background: '#b52323',
      margin: 0,
      padding: `${spacing.xsmall} ${spacing.small}`
    },

    label: {
      display: 'block',
      marginBottom: spacing.small,
      fontWeight: 400
    },

    row: {
      marginBottom: spacing.medium
    }
  };
}
