export default function button({ animation, border, colors, font, spacing }) {
  return {
    backgroundColor: colors.background,
    border: border.thinInvert,
    boxSizing: 'border-box',
    color: colors.foreground,
    display: 'inline-block',
    fontSize: font.default,
    font: 'inherit',
    letterSpacing: font.letterSpacing,
    lineHeight: '38px',
    margin: 0,
    overflow: 'visible',
    padding: `0 ${spacing.large}`,
    textAlign: 'center',
    textDecoration: 'none',
    textTransform: 'uppercase',
    transitionProperty: 'color,background-color,border-color',
    transition: animation.easeInOutMedium,
    verticalAlign: 'middle',
    outline: 'none',
    notDisabled: {
      cursor: 'pointer'
    },
    hover: {
      border: border.thinSecondary,
      background: colors.secondary,
      color: colors.foreground
    },
    focus: {
      border: border.thinSecondary,
      background: colors.secondary
    },
    active: {
      border: border.thinSecondary,
      background: colors.secondary
    },
    primary: {
      color: colors.primary,
      border: border.thinPrimary,
      hover: {
        border: {
          border: border.thinPrimary,
          background: colors.secondary,
          color: colors.foreground
        }
      },
      active: {
        border: {
          border: border.thinPrimary,
          background: colors.secondary,
          color: colors.foreground
        }
      },
      focus: {
        border: {
          border: border.thinPrimary,
          background: colors.secondary,
          color: colors.foreground
        }
      }
    }
  };
}
