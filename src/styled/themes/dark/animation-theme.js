export default function animation() {
  const xslow = '3s';
  const slow = '0.5s';
  const medium = '0.3s';
  const fast = '0.1s';

  return {
    xslow,
    slow,
    medium,
    fast,
    easeFast: `${fast} ease`,
    easeInFast: `${fast} ease-in-out`,
    easeInMedium: `${medium} ease-in-out`,
    easeInOutFast: `${fast} ease-in-out`,
    easeInOutMedium: `${medium} ease-in-out`,
    easeInOutSlow: `${slow} ease-in-out`,
    easeInOutXslow: `${xslow} ease-in-out`,
    easeInSlow: `${slow} ease-in-out`,
    easeInXslow: `${xslow} ease-in-out`,
    easeMedium: `${medium} ease`,
    easeSlow: `${slow} ease`,
    easeXslow: `${xslow} ease`
  };
}
