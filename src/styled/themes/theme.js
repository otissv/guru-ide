import dark from './dark/theme';

export default function theme(type) {
  const themes = {
    dark
  };

  return theme[type] || dark;
}
