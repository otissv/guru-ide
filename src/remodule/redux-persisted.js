import { IDE_ROUTE } from '../constants/routes-constants';
import { query } from '../helpers/async-query';
import removeListItem from '../helpers/remove-list-item';

export const register = 'persisted';

export const initialState = {
  activePersistedTabOrder: ['history'],
  persistedTabs: {
    history: {
      id: '',
      isHistory: true,
      collection: '',
      description: '',
      dirty: false,
      endpoint: 'http://localhost:8000/graphql',
      name: 'History',
      query: '',
      variables: '',
      results: {
        headers: {},
        request: {},
        response: {},
        status: '',
        time: ''
      }
    }
  },
  uiPersisted: {
    gqlTheme: 'dracula',
    gqlThemePaper: false,
    isInfoModalOpen: false,
    isSaveModalOpen: false,
    sidebarPersistedContent: 'left',
    route: ''
  },
  selectedPersisted: {
    id: null,
    collection: '',
    description: '',
    dirty: false,
    endpoint: 'http://localhost:8000/graphql',
    name: '',
    query: '',
    variables: '',
    results: {
      headers: {},
      request: {},
      response: '',
      status: 'Waiting...',
      time: ''
    }
  }
};

const IDE_PERSISTENT_FRAGMENT = `
  fragment idePersisted on IdePersisted {
    id
    projectId
    collection
    description
    endpoint
    name
    query
    created
    updated
    variables
    results
    RESULTS_ {
      result
      error {
        message
      }
    }
  }`;

export class RemoveTab {
  action(id) {
    return { type: 'RemoveTab', payload: id };
  }

  reducer(state, action) {
    const id = action.payload;
    const activePersistedTabOrder = removeListItem(
      [...state.activePersistedTabOrder],
      id
    );

    const persistedTabs = { ...state.persistedTabs };

    delete persistedTabs[id];

    return {
      ...state,
      activePersistedTabOrder,
      persistedTabs
    };
  }
}

export class SetActivePersistedTab {
  action(data) {
    return { type: 'SetActivePersistedTab', payload: data };
  }

  reducer(state, action) {
    const list = removeListItem(
      [...state.activePersistedTabOrder],
      action.payload
    );

    return {
      ...state,
      activePersistedTabOrder: [action.payload, ...list]
    };
  }
}

export class AddPersistedTabs {
  action(data, history) {
    return {
      type: 'AddPersistedTabs',
      payload: { [(history && 'history') || data.id]: data }
    };
  }

  reducer(state, action) {
    return {
      ...state,
      persistedTabs: { ...state.persistedTabs, ...action.payload }
    };
  }
}

// export class UpdatePersistedTab {
//   action(payload) {
//     return { type: 'UpdatePersistedTab', payload };
//   }

//   reducer(state, action) {
//     const { id } = action.payload;

//     return {
//       ...state,
//       persistedTabs: {
//         ...state.persistedTabs,
//         [id]: {
//           ...state.persistedTabs[id],
//           ...action.payload
//         }
//       }
//     };
//   }
// }

export class RemovePersistedTab {
  action(id) {
    return { type: 'RemovePersistedTab', payload: { id } };
  }

  reducer(state, action) {
    const payload = action.payload || {};
    const persistedTabs = { ...state.persistedTabs };
    delete persistedTabs[payload.id];

    const list = removeListItem(
      [...state.activePersistedTabOrder],
      action.payload.id
    );

    return {
      ...state,
      persistedTabs,
      activePersistedTabOrder: list
    };
  }
}

export class CreatePersisted {
  action(data) {
    const obj = {
      ...data,
      results: JSON.stringify(data.results)
    };

    const request = query({
      url: IDE_ROUTE,
      actions: ['idePersistedCreate'],
      query: `mutation (
        $id:          String
        $projectId:   String
        $collection:  String
        $description: String
        $endpoint:    String
        $name:        String
        $query:       String
        $variables:   String
        $results:     String
      ) {
        idePersistedCreate (
        id:          $id
        projectId:   $projectId
        collection:  $collection
        description: $description
        endpoint:    $endpoint
        name:        $name
        query:       $query
        variables:   $variables
        results:     $results
        ) {
          ...idePersisted
        }
      }
      ${IDE_PERSISTENT_FRAGMENT}`,
      variables: JSON.stringify(obj)
    });

    return { type: 'CreatePersisted', payload: request };
  }
}

export class GetPersisted {
  action() {
    const request = query({
      url: IDE_ROUTE,
      actions: ['idePersistedFindAll', 'idePersistedHistoryFindAll'],
      query: `query {
        idePersistedFindAll {
          ...idePersisted
        }
        idePersistedHistoryFindAll {
          ...idePersisted
          RESULTS_ {
            result
            error {
              type
              message
            }
          }
        }
      }
     ${IDE_PERSISTENT_FRAGMENT}`
    });

    return { type: 'GetPersisted', payload: request };
  }
}

export class SetUiPersisted {
  action(obj) {
    return { type: 'SetUiPersisted', payload: obj };
  }

  reducer(state, action) {
    if (action.type === 'SetUiPersisted') {
      return {
        ...state,
        uiPersisted: {
          ...state.uiPersisted,
          ...action.payload
        }
      };
    } else {
      return state;
    }
  }
}

export class SetPersistedResultProps {
  action(query) {
    return { type: 'SetPersistedResultProps', payload: query };
  }

  reducer(state, action) {
    return {
      ...state,
      selectedPersisted: {
        ...state.selectedPersisted,
        results: {
          ...state.selectedPersisted.results,
          ...action.payload
        }
      }
    };
  }
}

export class SelectedPersistedToInitialState {
  action() {
    return {
      type: 'SetSelectedPersisted',
      payload: initialState.selectedPersisted
    };
  }

  reducer(state, action) {
    return { ...state, selectedPersisted: action.payload };
  }
}

export class SetSelectedPersisted {
  action(query) {
    return { type: 'SetSelectedPersisted', payload: query };
  }

  reducer(state, action) {
    return { ...state, selectedPersisted: action.payload };
  }
}

export class SetSelectedPersistedProps {
  action(obj) {
    return { type: 'SetSelectedPersistedProps', payload: obj };
  }

  reducer(state, action) {
    if (action.type === 'SetSelectedPersistedProps') {
      return {
        ...state,
        selectedPersisted: {
          ...state.selectedPersisted,
          ...action.payload
        }
      };
    } else {
      return state;
    }
  }
}
