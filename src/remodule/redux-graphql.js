import { introspectionQuery } from 'graphql/utilities/introspectionQuery';
import { query, makeQuery } from '../helpers/async-query.js';

export const register = 'graphql';

export const initialState = {
  introspection: {},
  isConnected: false,
  builtSchema: {}
};

export class SetGraphql {
  action(payload) {
    return {
      type: 'SetGraphql',
      payload
    };
  }

  reducer(state, action) {
    return { ...state, ...action.payload };
  }
}

export class GetGraphqlSchema {
  action(endpoint) {
    const request = query({
      url: endpoint,
      actions: ['__schema'],
      query: introspectionQuery
    });

    return {
      type: 'GetGraphqlSchema',
      payload: request.then(response => response)
    };
  }
}

export class SetGraphqlSchema {
  action(schema) {
    return {
      type: 'SetGraphqlSchema',
      payload: schema
    };
  }

  reducer(state, action) {
    return { ...state, introspection: action.payload };
  }
}

export class SetSchemaIsConnected {
  action(boolean) {
    return {
      type: 'SetSchemaIsConnected',
      payload: boolean
    };
  }

  reducer(state, action) {
    return { ...state, isConnected: action.payload };
  }
}

export class FetchQuery {
  action({ url, operation }) {
    const request = query({
      url,
      actions: [operation.operation],
      query: makeQuery(operation),
      variables: JSON.stringify(operation.variables)
    });

    return { type: 'FetchQuery', payload: request };
  }
}

export class FetchMutation {
  action(payload) {
    return {
      type: 'FetchMutation',
      payload
    };
  }
}
