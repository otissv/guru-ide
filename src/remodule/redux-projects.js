import { IDE_ROUTE } from '../constants/routes-constants';
import { query } from '../helpers/async-query';

export const register = 'projects';

export const initialState = {
  project: {
    id: '',
    endpoint: 'http://localhost:8000/graphql',
    name: ''
  },
  projects: [],
  uiProjects: {
    show: false
  }
};

const IDE_PROJECT_FRAGMENT = `
fragment ideProject on IdeProject {
  id
  name
  headers
  endpoint
  RESULTS_ {
    result
    error {
      message
    }
  }
}`;

export class SetUiProjects {
  action(obj) {
    return { type: 'SetUiProjects', payload: obj };
  }

  reducer(state, action) {
    if (action.type === 'SetUiProjects') {
      return {
        ...state,
        uiProjects: {
          ...state.uiProjects,
          ...action.payload
        }
      };
    } else {
      return state;
    }
  }
}

export class SetProjects {
  action(payload) {
    return { type: 'SetProjects', payload };
  }

  reducer(state, action) {
    return {
      ...state,
      ...action.payload
    };
  }
}

export class GetProjects {
  action() {
    const request = query({
      url: IDE_ROUTE,
      actions: ['ideProjectFindAll'],
      query: `query {
        ideProjectFindAll {
          ...ideProject
        }
      }
      ${IDE_PROJECT_FRAGMENT}
      `
    });

    return { type: 'GetProjects', payload: request };
  }

  reducer(state, action) {
    return {
      ...state,
      projects: action.payload
    };
  }
}

export class CreateProject {
  action(payload) {
    const request = query({
      url: IDE_ROUTE,
      actions: ['ideProjectCreate'],
      query: `mutation (
        $id:       String,
        $name:     String,
        $headers:  String,
        $endpoint: String
      ) {
        ideProjectCreate (
          id: $id,
          name: $name,
          headers: $headers,
          endpoint: $endpoint
        ) {
          ...ideProject
        }
      }
      ${IDE_PROJECT_FRAGMENT}
      `,
      variables: JSON.stringify(payload, null, 2)
    });

    return { type: 'CreateProject', payload: request };
  }
}

export class GetProject {
  action(data) {
    const request = query({
      url: IDE_ROUTE,
      actions: ['ideProjectById'],
      query: `query {
        ideProjectById {
          ...ideProject
        }
      }
      ${IDE_PROJECT_FRAGMENT}
      `,
      variables: JSON.stringify(data)
    });

    return { type: 'GetProject', payload: request };
  }

  reducer(state, action) {
    return {
      ...state,
      projects: action.payload
    };
  }
}

export class SetEndpoint {
  action(bool) {
    return { type: 'SetEndpoint', payload: bool };
  }

  reducer(state, action) {
    return {
      ...state,
      project: {
        ...state.project,
        endpoint: action.payload
      }
    };
  }
}

export class UpdateProject {
  action(payload) {
    return { type: 'UpdateProject', payload };
  }

  reducer(state, action) {
    const { endpoint, headers, id, name } = action.payload;

    return {
      ...state,
      project: {
        id,
        endpoint,
        name,
        headers
      }
    };
  }
}
