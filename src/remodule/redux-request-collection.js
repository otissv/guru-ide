import * as reduxRequest from './redux-request';

export const register = 'requestCollection';

export const initialState = {
  requestCollection: {}
};

export class RequestCollectionAllReducer {
  reducer(state, action) {
    return {
      ...state,
      requestCollection: { ...state.requestCollection, ...action.payload }
    };
  }
}

export class AddRequestCollection {
  action(data) {
    const collection = {
      [data.collection]: {
        [data.id]: data
      }
    };

    return { type: 'RequestCollectionAllReducer', payload: collection };
  }
}

export class UpdateRequestCollection {
  action(payload) {
    return { type: 'UpdateRequestCollection', payload };
  }

  reducer(state, action) {
    const payload = action.payload;

    return {
      ...state,
      requestCollection: {
        ...state.requestCollection,
        [payload.collection]: {
          ...state.requestCollection[payload.collection],
          [payload.id]: payload
        }
      }
    };
  }
}

export class RequestCollectionAllToInitialState {
  action() {
    return {
      type: 'RequestCollectionAllToInitialState',
      payload: {}
    };
  }

  reducer(state, action) {
    return {
      ...state,
      requestCollection: initialState.requestCollection
    };
  }
}

export class CreateRequestCollections {
  action(collections) {
    const payload = collections.reduce((previous, item) => {
      const itemCollection = previous[item.collection] || {};

      return {
        ...previous,
        [item.collection]: {
          ...itemCollection,
          [item.id]: {
            ...item,
            results: item.results
              ? JSON.parse(JSON.parse(JSON.parse(item.results)))
              : reduxRequest.initialState.results
          }
        }
      };
    }, {});

    return { type: 'RequestCollectionAllReducer', payload };
  }
}
