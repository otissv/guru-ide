import * as reduxPersisted from './redux-persisted';

export const register = 'persistedCollection';

export const initialState = {
  persistedCollection: {}
};

export class PersistedCollectionReducer {
  reducer(state, action) {
    return {
      ...state,
      persistedCollection: { ...state.persistedCollection, ...action.payload }
    };
  }
}

export class AddPersistedCollection {
  action(data) {
    const collection = {
      [data.collection]: {
        [data.id]: data
      }
    };

    return { type: 'PersistedCollectionReducer', payload: collection };
  }
}

export class PersistedCollectionToInitialState {
  action() {
    return {
      type: 'PersistedCollectionToInitialState',
      payload: {}
    };
  }

  reducer(state, action) {
    return {
      ...state,
      persistedCollection: initialState.persistedCollection
    };
  }
}

export class CreatePersistedCollections {
  action(collections) {
    const payload = collections.reduce((previous, item) => {
      const itemCollection = previous[item.collection] || {};

      return {
        ...previous,
        [item.collection]: {
          ...itemCollection,
          [item.id]: {
            ...item,
            results: item.results
              ? JSON.parse(JSON.parse(JSON.parse(item.results)))
              : reduxPersisted.initialState.results
          }
        }
      };
    }, {});

    return { type: 'PersistedCollectionReducer', payload };
  }
}
