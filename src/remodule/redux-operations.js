import removeListItem from '../helpers/remove-list-item';

export const register = 'operations';

export const initialState = {
  activeOperationsTabOrder: [],
  operationTabs: {},
  uiOperations: {
    gqlTheme: 'dracula',
    gqlThemePaper: false,
    isInfoModalOpen: false,
    isSaveModalOpen: false,
    operationType: 'Query',
    route: '',
    sidebarOperationsContent: 'left'
  }
};

export class SetUiOperations {
  action(payload) {
    return { type: 'SetUiOperations', payload };
  }

  reducer(state, action) {
    return {
      ...state,
      uiOperations: { ...state.uiOperations, ...action.payload }
    };
  }
}

export class SetActiveOperationsTab {
  action(payload) {
    return { type: 'SetActiveOperationsTab', payload };
  }

  reducer(state, action) {
    const list = removeListItem(
      [...state.activeOperationsTabOrder],
      action.payload
    );

    return {
      ...state,
      activeOperationsTabOrder: [action.payload, ...list]
    };
  }
}

export class AddOperationTabResults {
  action(payload) {
    return {
      type: 'AddOperationTabResults',
      payload: payload
    };
  }

  reducer(state, action) {
    const { operation, prop } = action.payload;

    return {
      ...state,
      operationTabs: {
        ...state.operationTabs,
        [operation]: {
          ...state.operationTabs[operation],
          results: prop
        }
      }
    };
  }
}

export class AddOperationTabs {
  action(payload) {
    return { type: 'AddOperationTabs', payload };
  }

  reducer(state, action) {
    return {
      ...state,
      operationTabs: { ...state.operationTabs, ...action.payload }
    };
  }
}

export class AddOperationTabRequestProps {
  action(payload) {
    return { type: 'AddOperationTabRequestProps', payload };
  }

  reducer(state, action) {
    const { operation, prop } = action.payload;

    return {
      ...state,
      operationTabs: {
        ...state.operationTabs,
        [operation]: {
          ...state.operationTabs[operation],
          request: prop
        }
      }
    };
  }
}

export class AddOperationTabProps {
  action(payload) {
    return {
      type: 'AddOperationTabProps',
      payload: payload
    };
  }

  reducer(state, action) {
    const { query, operation } = action.payload;

    if (
      state.operationTabs[operation] &&
      state.operationTabs[operation].query
    ) {
      let args;
      let fields;

      if (query.args) {
        const argName = query.args.name;
        const argValue = query.args.value;
        const argType = Array.isArray(argValue) ? 'array' : typeof argValue;

        const type = {
          array: argState => [...argState, ...argValue],
          object: argState => ({ ...argState, ...argValue })
        };

        args = {
          ...state.operationTabs[operation].query.args,
          [argName]: type[argType]
            ? type[argType](state.operationTabs[operation].query.args[argName])
            : argValue
        };
      } else {
        args = state.operationTabs[operation].query.args || {};
      }

      fields = [
        ...state.operationTabs[operation].query.fields,
        query.fields
      ].filter(field => Boolean(field));

      return {
        ...state,
        operationTabs: {
          ...state.operationTabs,
          [operation]: {
            ...state.operationTabs[operation],
            query: {
              ...state.operationTabs[operation].query,
              fields: fields || [],
              args: args
            }
          }
        }
      };
    } else {
      return state;
    }
  }
}

export class RemoveOperationTabProps {
  action(payload) {
    return {
      type: 'RemoveOperationTabProps',
      payload: payload
    };
  }

  reducer(state, action) {
    const { arg, field, operation } = action.payload;

    if (
      state.operationTabs[operation] &&
      state.operationTabs[operation].query
    ) {
      const args = [...state.operationTabs[operation].query.args];
      const argsIndex = args.indexOf(arg);
      const fields = [...state.operationTabs[operation].query.fields];

      if (argsIndex >= 0) args.splice(argsIndex, 1);

      return {
        ...state,
        operationTabs: {
          ...state.operationTabs,
          [operation]: {
            ...state.operationTabs[operation],
            query: {
              ...state.operationTabs[operation].query,
              fields: fields.filter(f => f.name !== field),
              args
            }
          }
        }
      };
    } else {
      return state;
    }
  }
}

export class RemoveOperationTab {
  action(payload) {
    return { type: 'RemoveOperationTab', payload: payload };
  }

  reducer(state, action) {
    const { operationType, id } = action.payload;

    let operationTabsName;
    let operation;

    if (operationType === 'Query') {
      operationTabsName = 'operationTabs';
      operation = 'activeOperationsTabOrder';
    } else {
      operationTabsName = 'mutationOperationTabs';
      operation = 'activeMutationOperationsTabOrder';
    }

    const operationTabs = { ...state[operationTabsName] };
    delete operationTabs[id];

    const list = removeListItem([...state[operation]], id);

    return {
      ...state,
      [operationTabsName]: operationTabs,
      [operation]: list
    };
  }
}
