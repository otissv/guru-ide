import { IDE_ROUTE } from '../constants/routes-constants';
import { query } from '../helpers/async-query';

export const register = 'persistedHistory';

export const initialState = {
  persistedHistory: {}
};

export class PersistedHistoryReducer {
  reducer(state, action) {
    return {
      ...state,
      persistedHistory: {
        ...state.persistedHistory,
        ...action.payload
      }
    };
  }
}

export class AddPersistedHistoryItem {
  action(history) {
    return {
      type: 'PersistedHistoryReducer',
      payload: history
    };
  }
}

export class ClearPersistedHistoryItem {
  action(history) {
    return {
      type: 'PersistedHistoryReducer',
      payload: initialState.persistedHistory
    };
  }
}

export class CreatePersistedHistory {
  action(history) {
    const data = history.reduce(
      (previous, item) => ({
        ...previous,
        [item.id]: item
      }),
      {}
    );

    return { type: 'PersistedHistoryReducer', payload: data };
  }
}

export class PersistedHistoryToInitialState {
  action() {
    return {
      type: 'PersistedHistoryToInitialState',
      payload: {}
    };
  }

  reducer(state, action) {
    return {
      ...state,
      persistedHistory: initialState.persistedHistory
    };
  }
}

export class SavePersistedHistory {
  action(history) {
    const date = Date.parse(new Date());

    const data = {
      id: date,
      ...history,
      response: JSON.stringify(history.response)
    };

    const request = query({
      url: IDE_ROUTE,
      actions: ['idePersistedHistorySave'],
      query: `mutation (
        $id:        String
        $projectId: String
        $endpoint:  String
        $name:      String
        $query:     String
        $results:   String
        $variables: String
      ) {
        idePersistedHistorySave (
          id:        $id
          projectId: $projectId
          endpoint:  $endpoint
          name:      $name
          query:     $query
          results:   $results
          variables: $variables
        ) {
          RESULTS_ {
            result
          }
        }
      }`,
      variables: JSON.stringify(data)
    });

    return { type: 'SavePersistedHistory', payload: request };
  }
}
