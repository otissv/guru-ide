import { IDE_ROUTE } from '../constants/routes-constants';
import { query } from '../helpers/async-query';
import moment from 'moment';

export const register = 'requestHistory';

export const initialState = {
  requestHistory: {}
};

export class RequestHistoryAll {
  reducer(state, action) {
    return {
      ...state,
      requestHistory: { ...state.requestHistory, ...action.payload }
    };
  }
}

export class AddRequestHistoryItem {
  action(history) {
    return { type: 'RequestHistoryAll', payload: history };
  }
}

export class ClearRequestHistoryItem {
  action(history) {
    return {
      type: 'RequestHistoryAll',
      payload: initialState.requestHistory
    };
  }
}

export class CreateRequestHistory {
  action(history) {
    const data = history.reduce((previous, item) => {
      const date = new Date(parseInt(item.id, 10)).toISOString();

      return {
        ...previous,
        [item.id]: {
          ...item,
          isHistory: true,
          name: moment(date).format('L-HH:mm:ss'),
          results:
            typeof item.results === 'string'
              ? JSON.parse(item.results)
              : item.results
        }
      };
    }, {});

    return { type: 'RequestHistoryAll', payload: data };
  }
}

export class DeleteHistoryItem {
  action(id) {
    return { type: 'DeleteHistoryItem', payload: id };
  }

  reducer(state, action) {
    const requestHistory = { ...state.requestHistory };

    delete requestHistory[action.payload];

    return {
      ...state,
      requestHistory
    };
  }
}

export class RemoveHistoryItem {
  action(data) {
    const request = query({
      url: IDE_ROUTE,
      actions: ['ideRequestHistoryRemove'],
      query: `mutation (
        $id:        String
        $projectId: String
      ) {
        ideRequestHistoryRemove (
          id:        $id
          projectId: $projectId
        ) {
          RESULTS_ {
            result
          }
        }
      }`,
      variables: JSON.stringify(data)
    });

    return { type: 'RemoveHistoryItem', payload: request };
  }
}

export class requestHistoryAllToInitialState {
  action() {
    return {
      type: 'requestHistoryAllToInitialState',
      payload: {}
    };
  }

  reducer(state, action) {
    return {
      ...state,
      requestHistory: initialState.requestHistory
    };
  }
}

export class SaveRequestHistory {
  action(history) {
    const data = {
      ...history,
      id: typeof history.id === 'string' ? history.id : history.id.toString(),
      results: JSON.stringify(history.results, null, 2)
    };

    const request = query({
      url: IDE_ROUTE,
      actions: ['ideRequestHistorySave'],
      query: `mutation (
        $id:        String
        $projectId: String
        $endpoint:  String
        $name:      String
        $query:     String
        $results:   String
        $variables: String
      ) {
        ideRequestHistorySave (
          id:        $id
          projectId: $projectId
          endpoint:  $endpoint
          name:      $name
          query:     $query
          results:   $results
          variables: $variables
        ) {
          RESULTS_ {
            result
          }
        }
      }`,
      variables: JSON.stringify(data)
    });

    return { type: 'SaveRequestHistory', payload: request };
  }
}
