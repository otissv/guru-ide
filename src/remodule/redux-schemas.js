import { SCHEMA_ROUTE } from '../constants/routes-constants';
import axios from 'axios';
import { ROUTE_SCHEMAS_AST } from '../modules/router/route-constants';
export const register = 'schemas';

export const initialState = {
  schemas: {
    ast: {},
    print: '',
    type: [],
    definition: {}
  },
  uiSchemas: {
    route: ROUTE_SCHEMAS_AST
  }
};

export class GetSchemas {
  action() {
    const request = axios.get(SCHEMA_ROUTE);

    return {
      type: 'GetSchemas',
      payload: request.then(response => response)
    };
  }
}

export class SetUiSchemas {
  action(schemas) {
    return { type: 'SetUiSchemas', payload: schemas };
  }

  reducer(state, action) {
    return {
      ...state,
      uiSchemas: {
        ...state.uiSchemas,
        ...action.payload
      }
    };
  }
}

// export class SetSchemas {
//   action(schemas) {
//     return { type: 'SetSchemas', payload: schemas };
//   }

//   reducer(state, action) {
//     return { ...state, schemas: action.payload };
//   }
// }
