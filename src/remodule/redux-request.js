import { IDE_ROUTE } from '../constants/routes-constants';
import { query } from '../helpers/async-query';
import removeListItem from '../helpers/remove-list-item';

export const register = 'request';

export const initialState = {
  activeRequestTabOrder: ['history'],
  requestTabs: {
    history: {
      id: '',
      isHistory: true,
      collection: '',
      description: '',
      dirty: false,
      endpoint: 'http://localhost:8000/graphql',
      name: 'History',
      query: '',
      variables: '',
      results: {
        headers: {},
        request: {},
        response: {},
        status: '',
        time: ''
      }
    }
  },
  uiRequest: {
    gqlTheme: 'dracula',
    gqlThemePaper: false,
    isInfoModalOpen: false,
    isSaveModalOpen: false,
    sidebarRequestContent: 'left',
    route: ''
  }
};

const IDE_REQUEST_FRAGMENT = `
fragment ideRequest on IdeRequest {
  id
  collection
  created
  description
  endpoint
  name
  projectId
  query
  results
  variables
  RESULTS_ {
    result
    error {
      message
    }
  }
}`;

export class RemoveTab {
  action(id) {
    return { type: 'RemoveTab', payload: id };
  }

  reducer(state, action) {
    const id = action.payload;
    const activeRequestTabOrder = removeListItem(
      [...state.activeRequestTabOrder],
      id
    );

    const requestTabs = { ...state.requestTabs };

    delete requestTabs[id];

    return {
      ...state,
      activeRequestTabOrder,
      requestTabs
    };
  }
}

export class SetActiveRequestTab {
  action(data) {
    return { type: 'SetActiveRequestTab', payload: data };
  }

  reducer(state, action) {
    const list = removeListItem(
      [...state.activeRequestTabOrder],
      action.payload
    );

    return {
      ...state,
      activeRequestTabOrder: [action.payload, ...list]
    };
  }
}

export class AddRequestTabs {
  action(data, history) {
    return {
      type: 'AddRequestTabs',
      payload: { [(history && 'history') || data.id]: data }
    };
  }

  reducer(state, action) {
    return {
      ...state,
      requestTabs: { ...state.requestTabs, ...action.payload }
    };
  }
}

export class UpdateRequestTab {
  action(payload) {
    return { type: 'UpdateRequestTab', payload };
  }

  reducer(state, action) {
    const { id } = action.payload;

    return {
      ...state,
      requestTabs: {
        ...state.requestTabs,
        [id]: {
          ...state.requestTabs[id],
          ...action.payload
        }
      }
    };
  }
}

export class RemoveRequestTab {
  action(id) {
    return { type: 'RemoveRequestTab', payload: { id } };
  }

  reducer(state, action) {
    const payload = action.payload || {};
    const requestTabs = { ...state.requestTabs };
    delete requestTabs[payload.id];

    const list = removeListItem(
      [...state.activeRequestTabOrder],
      action.payload.id
    );

    return {
      ...state,
      requestTabs,
      activeRequestTabOrder: list
    };
  }
}

export class CreateRequest {
  action(data) {
    const obj = {
      ...data,
      results: JSON.stringify(data.results)
    };

    const request = query({
      url: IDE_ROUTE,
      actions: ['ideRequestCreate'],
      query: `mutation (
        $collection:  String
        $description: String
        $endpoint:    String
        $id:          String
        $name:        String
        $projectId:   String
        $query:       String
        $results:     String
        $variables:   String
      ) {
        ideRequestCreate (
          id:          $id
          collection:  $collection
          description: $description
          endpoint:    $endpoint
          name:        $name
          projectId:   $projectId
          query:       $query
          variables:   $variables
          results:     $results
        ) {
          ...ideRequest
        }
      }
      ${IDE_REQUEST_FRAGMENT}`,
      variables: JSON.stringify(obj)
    });

    return { type: 'CreateRequest', payload: request };
  }
}

export class GetRequest {
  action(projectId) {
    const request = query({
      url: IDE_ROUTE,
      actions: ['ideRequestFindAll', 'ideRequestHistoryFind'],
      query: `query ($projectId: String) {
        ideRequestFindAll (projectId: $projectId) {
          ...ideRequest
        }
        ideRequestHistoryFind (projectId: $projectId){
          id
          projectId
          description
          endpoint
          name
          query
          variables
          results
          RESULTS_ {
            result
          }
        }
      }
      ${IDE_REQUEST_FRAGMENT}`,
      variables: JSON.stringify({ projectId })
    });

    return { type: 'GetRequest', payload: request };
  }
}

export class setUiRequest {
  action(contentType) {
    return { type: 'setUiRequest', payload: contentType };
  }

  reducer(state, action) {
    return {
      ...state,
      uiRequest: { ...state.uiRequest, ...action.payload }
    };
  }
}
