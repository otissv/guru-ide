export const register = 'navigation';

export const initialState = {
  uiNavigation: {
    route: '/',
    show: false
  }
};

export class SetUiNavProps {
  action(obj) {
    return { type: 'SetUiNavProps', payload: obj };
  }

  reducer(state, action) {
    return {
      ...state,
      uiNavigation: {
        ...state.uiNavigation,
        ...action.payload
      }
    };
  }
}
