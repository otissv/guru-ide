export const register = 'app';

export const initialState = {
  title: ''
};

export class SetTitle {
  action(payload) {
    return { type: 'SetTitle', payload };
  }

  reducer(state, action) {
    return { ...state, title: action.payload };
  }
}
