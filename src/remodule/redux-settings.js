import { query as gqlFetch } from '../helpers/async-query';
import { IDE_ROUTE } from '../constants/routes-constants';
import { ROUTE_SETTINGS_DATABASE_JSONDB } from '../modules/router/route-constants';
export const register = 'settings';

export const initialState = {
  uiSettings: {
    route: '',
    databaseRoute: ROUTE_SETTINGS_DATABASE_JSONDB
  }
};

export class ClearSettingsHistory {
  action(history = {}) {
    let request = Promise.resolve();

    if (
      history.persistedCollection ||
      history.persistedHistory ||
      history.queryCollection ||
      history.queryHistory
    ) {
      const idePersistedCollectionClear = history.persistedCollection
        ? 'idePersistedCollectionClear { id }'
        : '';
      const idePersistedHistoryClear = history.persistedHistory
        ? 'idePersistedHistoryClear { id }'
        : '';
      const ideRequestHistoryClear = history.queryHistory
        ? 'ideRequestHistoryClear { id }'
        : '';
      const ideRequestCollectionClear = history.queryCollection
        ? 'ideRequestCollectionClear { id }'
        : '';

      const actions = [
        idePersistedCollectionClear,
        idePersistedHistoryClear,
        ideRequestCollectionClear,
        ideRequestHistoryClear
      ].reduce((previous, action) => {
        return action !== '' ? [...previous, action] : previous;
      }, []);

      const query = `mutation {
        ${idePersistedCollectionClear}
        ${idePersistedHistoryClear}
        ${ideRequestHistoryClear}
        ${ideRequestCollectionClear}
      }`;

      request = gqlFetch({
        url: IDE_ROUTE,
        actions,
        query
      });
    }

    return { type: 'DoNothing', payload: request };
  }
}

export class SetUiSettingsProps {
  action(bool) {
    return { type: 'SetUiSettingsProps', payload: bool };
  }

  reducer(state, action) {
    return {
      ...state,
      uiSettings: { ...state.uiSettings, ...action.payload }
    };
  }
}
