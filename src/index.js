import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import routes from './modules/router/routes';
import init from './core/global/init-global';
import store from './store';
import './index.css';
import Theme from './core/theme/components/Theme';

window.app = window.app || {};

init({ app: window.app, store });

ReactDOM.render(
  <Provider store={store}>
    <Theme>
      <BrowserRouter>{renderRoutes(routes)}</BrowserRouter>
    </Theme>
  </Provider>,
  document.getElementById('root')
);
