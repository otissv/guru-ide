import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import getClassMethods from '../../../helpers/get-class-methods';
import Accordion from '../../../styled/components/accordion/Accordion';
import AccordionButton from '../../../styled/components/accordion/Accordion-button';
import AccordionContent from '../../../styled/components/accordion/Accordion-content';
import AccordionItem from '../../../styled/components/accordion/Accordion-item';
import List from '../../../styled/components/list/List';
import ListItem from '../../../styled/components/list/ListItem';
import ListItemNavLink from '../../../styled/components/list/ListItemNavLink';
import Sidebar from '../../../styled/components/sidebar/Sidebar';
import Svg from '../../../styled/components/Svg';
import Tabs from '../../../styled/components/tabs/Tabs';
import archiveIcon from '../../../icons/archive.svg';
import fileDownload from 'js-file-download';
import historyIcon from '../../../icons/back-in-time.svg';
import moment from 'moment';
import mutationIcon from '../../../icons/new-message.svg';
import queryIcon from '../../../icons/text-document.svg';

const Container = styled.div`
  display: flex;
  margin-top: 10px;
`;

const Main = styled.div`
  display: flex;
  flex: 1;
`;

const Title = styled.h2`
  margin: 0 0 20px 0;
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Label = styled.div``;
const Meta = styled.div``;

const HistoryInfo = props => (
  <div>
    <p> Use the query editor to left to send queries to the server. </p>
    <p>
      Queries typically start with a {`"{"`} character. Lines that start with a#
      are ignored.
    </p>
    <p> An example query might look like: </p>
    <pre>
      {' '}
      {`}
  User {
    id
    firstName
    lastName
  }
}`}
    </pre>

    <p>
      Keyboard shortcuts: <br />
      <br />
      Run Query: Ctrl - Enter or press the play button above the query editor.{' '}
      <br />
      <br />
      Auto Complete: Ctrl - Space or just start typing.
    </p>
  </div>
);

const CollectionInfo = props => <p>Save queries to create collections</p>;

export default class Request extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);

    this.state = {
      accordion: {}
    };
  }
  componentWillMount() {
    const {
      location: { pathname },
      setUiRequest,
      uiRequest: { route }
    } = this.props;
    const pathSplit = pathname.split('/');
    const id = pathSplit[pathSplit.length - 1];

    if (route.trim() === '') {
      setUiRequest({ route: pathname });
    }
  }

  createCollectionRoutes(collections) {
    return Object.keys(collections).reduce((previous, key) => {
      const collection = collections[key];

      return [
        ...previous,
        ...Object.keys(collection).map(id => {
          return {
            path: `/request/collection/${key}/${id}`,
            label: collection[id].name,
            type: 'collection',
            id,
            collection: key
          };
        })
      ];
    }, []);
  }

  createHistoryRoutes(history) {
    return [
      ...Object.keys(history)
        .reverse()
        .map(key => {
          const date = new Date(parseInt(key, 10)).toISOString();

          return {
            path: `/request/history/${key}`,
            label: moment(date).format('L-HH:mm:ss'),
            type: 'history',
            id: key
          };
        })
    ];
  }

  getCollectionSidebarItems() {
    const { activeRequestTabOrder, requestCollection } = this.props;
    const active = activeRequestTabOrder[0];
    const routes = this.createCollectionRoutes(requestCollection);

    return Object.keys(requestCollection).map(key => {
      const items = routes
        .filter(route => route.collection === key)
        .map(item => {
          return this.getListItems({
            active,
            id: item.id,
            item,
            onClick: this.handleRequestCollectionItemClick
          });
        });

      return (
        <AccordionItem className="Collection-accordion-item" key={key}>
          <AccordionButton
            className="Collection-accordion-button"
            data-collection={key}
            kitid={key}
            onClick={this.handleOnAccordionClicked}
            opened={this.state.accordion[key]}
          >
            {key} {this.state.accordion[key]}
          </AccordionButton>
          <AccordionContent
            className="Collection-accordion-content"
            opened={this.state.accordion[key]}
          >
            {items}
          </AccordionContent>
        </AccordionItem>
      );
    });
  }

  getHistorySidebarItems() {
    const {
      activeRequestTabOrder,
      addRequestTabs,
      requestHistory,
      requestTabs,
      location: { pathname },
      uiRequest: { route }
    } = this.props;

    const active =
      activeRequestTabOrder[0] === 'history'
        ? requestTabs.history.id
        : activeRequestTabOrder[0];
    const routes = this.createHistoryRoutes(requestHistory);

    return routes.filter(route => route.label).map(item => {
      const pathSplit = item.path.split('/');
      const id = pathSplit[pathSplit.length - 1];
      const query = requestHistory[id].query;

      return this.getListItems({
        active,
        onClick: this.handleOnHistoryItemClick,
        id,
        item,
        query
      });
    });
  }

  getListItems({ active, id, item, query, onClick }) {
    return (
      <ListItem key={item.path}>
        <ListItemNavLink
          to={item.path}
          data-route={item.path}
          data-active={id === active}
          onClick={onClick}
          data-type={item.type}
          data-collection={item.collection}
        >
          {item.icon && (
            <Svg small styledSvg="margin-right: 5px" src={item.icon} />
          )}
          <Label>{item.label}</Label>
          {query && <Meta>{`${query.substr(0, 28)}...`}</Meta>}
        </ListItemNavLink>
      </ListItem>
    );
  }

  handleRequestCollectionItemClick(event) {
    const { handleRequestCollectionItemClick } = this.props;

    handleRequestCollectionItemClick({
      ...event.currentTarget.dataset
    });
  }

  handleOnHistoryItemClick(event) {
    const { handleOnHistoryItemClick } = this.props;

    handleOnHistoryItemClick({ ...event.currentTarget.dataset });
  }

  handleOnAccordionClicked(event) {
    const collection =
      event.target.dataset.collection || event.currentTarget.dataset.collection;
    const bool = !this.state.accordion[collection] || false;

    this.setState({
      accordion: {
        ...this.state.accordion,
        [collection]: bool
      }
    });
  }

  render() {
    const { component } = this.props;
    const Component = component;

    return <Component {...getClassMethods(this)} />;
  }
}
