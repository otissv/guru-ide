import React from 'react';
import styled from 'styled-components';
import autobind from 'class-autobind';
import List from '../../../styled/components/list/List';
import ListItem from '../../../styled/components/list/ListItem';
import ListItemNavLink from '../../../styled/components/list/ListItemNavLink';
import Sidebar from '../../../styled/components/sidebar/Sidebar';
import Svg from '../../../styled/components/Svg';


const Container = styled.div`
  background: ${props => props.theme.colors.background};
  flex: 1;
  display: flex;
  flex-direction: column;

  ${props => props.styledContentLayout};
`;

const Heading = styled.h2`margin: 10px 0 20px 0;`;

const Body = styled.div`
  display: flex;
  flex: 1;
`;

const Main = styled.div`padding: 10px 10px 0 10px; flex 1; display: flex`;

export default class Databases extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  handelOnSidebarItemClick(event) {
    const { sidebar: { onClick } } = this.props;

    onClick(event.target.dataset);
  }

  getSidebarItems() {
    const { active, routes } = this.props;

    return (
      routes &&
      routes.filter(route => route.label).map(item => {
        return (
          <ListItem key={item.path}>
            <ListItemNavLink
              to={item.path}
              data-route={item.path}
              data-active={item.path === active}
              onClick={this.handelOnSidebarItemClick}
              data-type={item.type}
            >
              {item.icon && (
                <Svg styledSvg="margin-right: 5px" src={item.icon} />
              )}{' '}
              {item.label}
            </ListItemNavLink>
          </ListItem>
        );
      })
    );
  }

  render() {
    const {
      children,
      className,
      heading,
      renderRoutes,
      sidebar,
      styledContentLayout
    } = this.props;
    const buttonLeft = sidebar && sidebar.buttonLeft;
    const buttonRight = sidebar && sidebar.buttonRight;

    return (
      <Container
        className={className || 'Content-layout-container'}
        styledContentLayout={styledContentLayout}
      >
        {heading ? <Heading>{heading}</Heading> : null}
        <Body className="Content-layout-body">
          {sidebar ? (
            <Sidebar {...sidebar}>
              {!buttonLeft &&
                !buttonRight && (
                  <List styledList="padding-top: 0;">
                    {this.getSidebarItems()}
                  </List>
                )}
            </Sidebar>
          ) : null}

          <Main>
            {children}
            {renderRoutes}
          </Main>
        </Body>
      </Container>
    );
  }
}
