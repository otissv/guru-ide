import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import Button from '../../../styled/components/Button';
import Svg from '../../../styled/components/Svg';
import downloadIcon from '../../../icons/download.svg';
import fileDownload from 'js-file-download';

const DownloadButtonStyled = props => (
  <Button {...props}>
    <Svg styledSvg="transform: translate(-6px, 3px);" src={downloadIcon} />
    {props.children}
  </Button>
);

const DownloadButton = styled(DownloadButtonStyled)`
  padding: 0 10px;
  text-align: left;
`;

export default class Download extends PureComponent {
  constructor() {
    super(...arguments);
    autobind(this);
    this.editor = null;
    this.value = '';
  }

  handleOnDownloadClick(event) {
    const { data, fileName } = this.props;

    const stringify =
      data && typeof data === 'string' ? data : JSON.stringify(data, null, 2);

    if (stringify && stringify.trim() !== '') {
      fileDownload(stringify, fileName);
    }
  }

  render() {
    return (
      <DownloadButton onClick={this.handleOnDownloadClick}>
        {this.props.children}
      </DownloadButton>
    );
  }
}
