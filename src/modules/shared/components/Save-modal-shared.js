import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import Button from '../../../styled/components/Button';
import FormError from '../../../styled/components/form/FormError';
import FormLabel from '../../../styled/components/form/FormLabel';
import FormRow from '../../../styled/components/form/FormRow';
import Input from '../../../styled/components/Input';
import Modal from '../../../styled/components/Modal';
import ReduxForm from '../../shared/components/ReduxForm-shared';
import Select from '../../../styled/components/Select';
import Textarea from '../../../styled/components/Textarea';
import cuid from 'cuid';

const footerStyled = `
      display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: flex-end;
`;

export default class SaveModal extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  handleOnClickCancel(event) {
    event.preventDefault();
    const { setSaveModel } = this.props;
    setSaveModel();
  }

  render() {
    const {
      collectionLabels,
      forms,
      onSaveClick,
      onOptionChange,
      opened,
      saveModalHeader,
      setSaveModel,
      validation
    } = this.props;

    return (
      <Modal
        cancel={{ onClick: this.handleOnClickCancel }}
        id={cuid()}
        header={saveModalHeader}
        opened={opened}
        setVisibility={setSaveModel}
      >
        <ReduxForm
          name="saveForm"
          onSubmit={onSaveClick}
          validation={validation}
        >
          <FormRow>
            <FormLabel htmlFor="name">Name</FormLabel>

            <Input name="name" value={forms.saveForm.fields.name.value} />

            <FormError
              display={forms.saveForm.fields.name.error ? 'block' : 'none'}
            >
              {forms.saveForm.fields.name.error}
            </FormError>
          </FormRow>

          <FormRow>
            <FormLabel htmlFor="name">Collection</FormLabel>

            <Select
              name="collection"
              value={forms.saveForm.fields.collection.value}
              options={collectionLabels}
              onOptionChange={onOptionChange}
            />

            <FormError
              display={
                forms.saveForm.fields.collection.error ? 'block' : 'none'
              }
            >
              {forms.saveForm.fields.name.error}
            </FormError>
          </FormRow>

          <FormRow>
            <FormLabel htmlFor="name">Query Description (optional)</FormLabel>

            <Textarea
              name="description"
              value={forms.saveForm.fields.description.value}
              placeholder="Adding a description makes your docs better"
            />

            <FormError
              display={
                forms.saveForm.fields.description.error ? 'block' : 'none'
              }
            >
              {forms.saveForm.fields.description.error}
            </FormError>
          </FormRow>

          <FormRow styledFormRow={footerStyled}>
            <Button
              styledButton="margin-right: 10px;"
              onClick={this.handleOnClickCancel}
            >
              Cancel
            </Button>

            <Button primary type="submit">
              Save
            </Button>
          </FormRow>
        </ReduxForm>
      </Modal>
    );
  }
}
