import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import CopyToClipboard from 'react-copy-to-clipboard';
import IconButton from '../../../styled/components/IconButton';
import Modal from '../../../styled/components/Modal';
import copy from '../../../icons/copy.svg';
import styled from 'styled-components';

const Label = styled.label`
  display: inline-block;
  font-weight: 700;
  font-size: 12px;
  color: ${props => props.theme.colors.foreground};
  line-height: 1.6;
  width: 160px;
`;

const SubHeading = styled.h6`
  font-size: 14px;
  font-weight: 400;
  margin-bottom: ${props => props.theme.spacing.xsmall};
`;

const Copy = styled.div`display: inline;`;

const Copied = styled.div`
  display: inline;
  transition: ${props => 'opacity ' + props.theme.animation.easeMedium};
  font-size: ${props => props.theme.font.xsmall};
  margin-left: ${props => props.theme.spacing.xsmall};
`;

export default class InfoModal extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);

    this.state = {
      copied: false
    };
  }

  getID() {
    const { id, projectId } = this.props;

    return (
      <CopyToClipboard text={`${projectId}-${id}`}>
        <Copy>
          <Label>id: </Label>
          {`${projectId}-${id}`} <IconButton src={copy} />
          <Copied style={{ opacity: this.state.copied ? 1 : 0 }}>copied</Copied>
        </Copy>
      </CopyToClipboard>
    );
  }

  render() {
    const {
      id,
      infoModalHeader,
      isHistory,
      name,
      opened,
      results,
      setInfoModal
    } = this.props;

    const { headers, request, status, time } = results || {};

    const responseHeaderKeys = headers ? Object.keys(headers) : [];

    const responseHeaderItems =
      responseHeaderKeys.length > 0 &&
      responseHeaderKeys.map(key => {
        return (
          <div key={key}>
            <Label>{key}: </Label> {headers[key]}
          </div>
        );
      });

    const requestHeaderKeys =
      request && request.headers ? Object.keys(request.headers) : [];

    const requestHeaderItems =
      requestHeaderKeys > 0 &&
      requestHeaderKeys.map(key => {
        return (
          <div key={key}>
            <Label>{key}: </Label> {request.headers[key]}
          </div>
        );
      });

    return (
      <Modal
        setVisibility={setInfoModal}
        id={id}
        header={infoModalHeader}
        opened={opened}
        cancel={{ body: 'Close' }}
        modalClickClose
      >
        <SubHeading>
          {isHistory && 'History - '} {name}
        </SubHeading>

        {!isHistory && this.getID()}

        <SubHeading>General</SubHeading>
        <div>
          <Label>Request URL: </Label> {request && request.url}
        </div>
        <div>
          <Label>Request Method: </Label> POST
        </div>
        <div>
          <Label>Status Code:</Label> {status}
        </div>
        <div>
          <Label>Duration: </Label> {time} ms
        </div>

        <SubHeading>Request Headers</SubHeading>
        {requestHeaderItems}

        <SubHeading>Response Headers</SubHeading>
        {responseHeaderItems}
      </Modal>
    );
  }
}
