import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import moment from 'moment';
import historyIcon from '../../../../icons/back-in-time.svg';
import archiveIcon from '../../../../icons/archive.svg';
import Sidebar from '../../../../styled/components/sidebar/Sidebar';
import SideBarItem from './Editor-sidebar-item-shared';
import List from '../../../../styled/components/list/List';
import AccordionButton from '../../../../styled/components/accordion/Accordion-button';
import Accordion from '../../../../styled/components/accordion/Accordion';
import AccordionItem from '../../../../styled/components/accordion/Accordion-item';
import AccordionContent from '../../../../styled/components/accordion/Accordion-content';

const Info = styled.div`padding: 0 20px 20px 20px;`;

export default class EditorSidebar extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      active: '',
      accordion: {}
    };
  }

  collectionList() {
    const { collections, collectionInfo } = this.props;
    const collectionKeys = Object.keys(collections);

    const queries = items =>
      Object.keys(items).map(key => {
        const item = items[key];

        return (
          <SideBarItem
            styledSideBarItemLink="padding-left: 20px"
            active={this.state.active}
            collection={item.collection}
            item={item}
            key={key}
            kitid={item.id}
            label={item.name}
            onItemClick={this.handleOnCollectionItemClick}
          />
        );
      });

    return collectionKeys.length === 0 ? (
      <Info>{collectionInfo}</Info>
    ) : (
      collectionKeys.map(key => (
        <AccordionItem key={key}>
          <AccordionButton
            data-collection={key}
            kitid={key}
            onClick={this.handleOnAccordionClicked}
            opened={this.state.accordion[key]}
          >
            {key} {this.state.accordion[key]}
          </AccordionButton>
          <AccordionContent opened={this.state.accordion[key]}>
            {queries(collections[key])}
          </AccordionContent>
        </AccordionItem>
      ))
    );
  }

  handleOnCollectionItemClick(event) {
    const { handleOnCollectionItemClick } = this.props;
    const id = event.currentTarget.dataset.kitid;
    const collection = event.currentTarget.dataset.collection;

    this.setState({ active: id });
    handleOnCollectionItemClick({ id, collection });
  }

  handleOnHistoryItemClick(event) {
    const { handleOnHistoryItemClick } = this.props;
    const id = event.currentTarget.dataset.kitid;

    this.setState({ active: id });
    handleOnHistoryItemClick(id);
  }

  handleOnAccordionClicked(event) {
    const collection =
      event.target.dataset.collection || event.currentTarget.dataset.collection;
    const bool = !this.state.accordion[collection] || false;

    this.setState({
      accordion: {
        ...this.state.accordion,
        [collection]: bool
      }
    });
  }

  historyList() {
    const { history, historyInfo } = this.props;
    const historyKeys = Object.keys(history);

    return historyKeys.length === 0 ? (
      <Info key="info">{historyInfo}</Info>
    ) : (
      historyKeys.reverse().map(key => {
        const item = history[key];
        const date = new Date(parseInt(key, 10)).toISOString();

        return (
          <SideBarItem
            active={this.state.active}
            date={date}
            item={item}
            key={key}
            kitid={key}
            label={moment(date).format('L-HH:mm:ss')}
            meta={item.query}
            onItemClick={this.handleOnHistoryItemClick}
          />
        );
      })
    );
  }

  render() {
    const collections = this.collectionList();
    const history = this.historyList();
    const { showSidebarCollection, showSidebarHistory, type } = this.props;

    const left = (
      <List
        type="side"
        style={{ display: type === 'history' ? 'block' : 'none' }}
      >
        {history}
      </List>
    );

    const right = (
      <Accordion
        type="side"
        style={{
          display: type === 'collection' ? 'block' : 'none'
        }}
      >
        {collections}
      </Accordion>
    );
    return (
      <Sidebar
        buttonLeft={{
          component: left,
          icon: historyIcon,
          onClick: showSidebarHistory,
          title: 'History',
          type: 'history'
        }}
        buttonRight={{
          component: right,
          icon: archiveIcon,
          onClick: showSidebarCollection,
          title: 'Collection',
          type: 'collection'
        }}
      />
    );
  }
}
