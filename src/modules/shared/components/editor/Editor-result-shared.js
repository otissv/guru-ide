import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import CodeMirror from 'react-codemirror';
import styled from 'styled-components';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/addon/fold/foldgutter';
import 'codemirror/addon/fold/brace-fold';
import 'codemirror/addon/dialog/dialog';
import 'codemirror/addon/search/search';
import 'codemirror/keymap/sublime';
import '../../../shared/css/style-codemirrior-shared.css';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Result = styled.div`
  flex: 1;
  overflow-y: auto;
`;

export default class ResultEditor extends PureComponent {
  constructor() {
    super(...arguments);
    autobind(this);
    this.editor = null;
    this.value = '';
  }

  handleOnchange() {
    const value = this.editor.getCodeMirror().doc.getValue();
    this.props.handleOnChange(value);
  }

  render() {
    const { results } = this.props;

    return (
      <Container>
        <Result>
          <CodeMirror
            ref={editor => {
              this.editor = editor;
            }}
            preserveScrollPosition={true}
            value={results}
            autoSave={true}
            options={{
              autoCloseBrackets: true,
              extraKeys: {
                'Ctrl-Left': 'goSubwordLeft',
                'Ctrl-Right': 'goSubwordRight',
                'Alt-Left': 'goGroupLeft',
                'Alt-Right': 'goGroupRight'
              },
              foldGutter: {
                minFoldSize: 4
              },
              gutters: ['CodeMirror-foldgutter'],
              keyMap: 'sublime',
              lineWrapping: true,
              matchBrackets: true,
              mode: 'application/json',
              readOnly: true,
              tabSize: 2,
              theme: 'dracula'
            }}
          />
        </Result>
      </Container>
    );
  }
}
