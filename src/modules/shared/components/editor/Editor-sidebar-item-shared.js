import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import theme from '../../../../styled/theme';

import ListItem from '../../../../styled/components/list/ListItem';
import ListItemLink from '../../../../styled/components/list/ListItemLink';

const Label = styled.span`
  display: inline-block;
  font-size: ${props => props.theme.font.default};
  color: ${props => props.theme.colors.foreground};
`;
const Meta = styled.span`
  font-size: ${props => props.theme.font.default};
  display: inline-block;
  color: ${props => props.theme.colors.foreground};
`;

export default class SideBarItem extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      hover: false
    };
  }

  handleOnListItemMouseOver() {
    this.setState({ hover: true });
  }

  handleOnListItemMouseLeave() {
    this.setState({ hover: false });
  }

  render() {
    const {
      active,
      item,
      kitid,
      label,
      meta,
      onItemClick,
      styledSideBarItem,
      styledSideBarItemLink
    } = this.props;

    return (
      <div>
        <ListItem styledListItemLink={styledSideBarItem}>
          <ListItemLink
            active={kitid === active}
            data-kitid={kitid}
            data-collection={item.collection}
            onClick={onItemClick}
            onMouseLeave={this.handleOnListItemMouseLeave}
            onMouseOver={this.handleOnListItemMouseOver}
            styledListItemLink={`padding: 10px 10px; ${styledSideBarItemLink}`}
          >
            <Label active={kitid === active}>{label}</Label>
            <Meta hover={this.state.hover}>
              {meta ? `${item.query.substr(0, 30)}...` : null}
            </Meta>
          </ListItemLink>
        </ListItem>
      </div>
    );
  }
}
