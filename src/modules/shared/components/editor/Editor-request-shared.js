import React from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import CodeMirror from 'react-codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/addon/fold/foldgutter';
import 'codemirror/addon/fold/brace-fold';
import 'codemirror/addon/dialog/dialog';
import 'codemirror/addon/search/search';
import 'codemirror/keymap/sublime';
import '../../../shared/css/style-codemirrior-shared.css';

const PersistedRequest = styled.div`
  overflow-y: auto;
  flex: 1;
`;

export default class PersistedQueryEditor extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);
    this.editor = null;
    this.value = '';
  }

  handleOnchange() {
    const value = this.editor.getCodeMirror().doc.getValue();
    this.props.handleOnChange(value);
  }

  render() {
    const { query } = this.props;

    return (
      <PersistedRequest>
        <CodeMirror
          ref={editor => {
            this.editor = editor;
          }}
          autoFocus={true}
          autoSave={true}
          onChange={this.handleOnchange}
          preserveScrollPosition={true}
          value={query}
          options={{
            autoCloseBrackets: true,
            extraKeys: {
              'Ctrl-Left': 'goSubwordLeft',
              'Ctrl-Right': 'goSubwordRight',
              'Alt-Left': 'goGroupLeft',
              'Alt-Right': 'goGroupRight'
            },
            foldGutter: {
              minFoldSize: 4
            },
            gutters: ['CodeMirror-foldgutter'],
            keyMap: 'sublime',
            lineNumbers: true,
            matchBrackets: true,
            mode: 'application/json',
            json: true,
            showCursorWhenSelecting: true,
            tabSize: 2,
            theme: 'dracula'
          }}
        />
      </PersistedRequest>
    );
  }
}
