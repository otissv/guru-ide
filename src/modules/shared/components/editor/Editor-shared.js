import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import EditorSideBar from './Editor-sidebar-shared';
import InfoModal from '../Info-modal-shared';
import SaveModal from '../Save-modal-shared';
import Settings from '../../../settings/containers/container-settings';
import Tabs from '../../../../styled/components/tabs/Tabs';

const EditorContainer = styled.div`display: flex;`;

const Main = styled.div`
  display: flex;
  flex: 1;
`;

export default class Editor extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  collectionLabels() {
    return Object.keys(this.props.collections).map(key => ({
      label: key,
      value: key
    }));
  }

  render() {
    const {
      activeOrder,
      collection,
      collectionInfo,
      collections,
      component,
      forms,
      handleClickSave,
      handleOnCloseClick,
      handleOnHistoryRemoveClick,
      handleTabOnChange,
      history,
      historyInfo,
      infoModalHeader,
      infoModalOpened,
      items,
      handleOnCollectionItemClick,
      handleOnHistoryItemClick,
      saveModalHeader,
      saveModalOpened,
      setInfoModal,
      setSaveFormFields,
      setSaveModel,
      showSidebarCollection,
      showSidebarHistory,
      sidebarType,
      validation
    } = this.props;

    const activeId = activeOrder[0];

    return (
      <EditorContainer>
        <EditorSideBar
          collectionInfo={collectionInfo}
          collections={collections}
          handleOnHistoryRemoveClick={handleOnHistoryRemoveClick}
          history={history}
          historyInfo={historyInfo}
          handleOnCollectionItemClick={handleOnCollectionItemClick}
          handleOnHistoryItemClick={handleOnHistoryItemClick}
          showSidebarCollection={showSidebarCollection}
          showSidebarHistory={showSidebarHistory}
          type={sidebarType}
        />
        <Main>
          <Tabs
            component={component}
            items={items}
            active={activeId}
            onChange={handleTabOnChange}
            handleOnCloseClick={handleOnCloseClick}
          />

          <SaveModal
            collection={collection}
            collectionLabels={this.collectionLabels()}
            forms={forms}
            handleClickSave={handleClickSave}
            opened={saveModalOpened}
            saveModalHeader={saveModalHeader}
            setSaveFormFields={setSaveFormFields}
            setSaveModel={setSaveModel}
            validation={validation}
          />

          <InfoModal
            infoModalHeader={infoModalHeader}
            opened={infoModalOpened}
            selectedItem={items[activeId]}
            setInfoModal={setInfoModal}
          />
          <Settings />
        </Main>
      </EditorContainer>
    );
  }
}
