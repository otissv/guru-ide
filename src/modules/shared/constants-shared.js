export const TAB_RESET_STATE = {
  dirty: true,
  query: '',
  variables: '',
  results: {
    headers: {},
    request: {},
    response: {},
    status: '',
    time: ''
  }
};
