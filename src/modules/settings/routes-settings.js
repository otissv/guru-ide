import React from 'react';
import Settings from './components/Settings';
import SettingsContainer from './containers/container-settings';
import Project from './components/Project-settings';
import History from './components/History-settings';

import {
  ROUTE_SETTINGS,
  ROUTE_SETTINGS_HISTORY,
  ROUTE_SETTINGS_PROJECT
} from '../router/route-constants';

export default {
  [ROUTE_SETTINGS]: {
    component: props => <SettingsContainer {...props} component={Settings} />,
    routes: [
      {
        label: 'Project',
        path: ROUTE_SETTINGS_PROJECT,
        component: Project,
        active: true
      },
      {
        label: 'History',
        path: ROUTE_SETTINGS_HISTORY,
        component: History
      }
    ]
  }
};
