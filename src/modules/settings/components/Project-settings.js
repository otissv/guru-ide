import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import Button from '../../../styled/components/Button';
import ContentLayout from '../../shared/components/Content-layout-shared';
import FormError from '../../../styled/components/form/FormError';
import FormLabel from '../../../styled/components/form/FormLabel';
import FormRow from '../../../styled/components/form/FormRow';
import Input from '../../../styled/components/Input';
import ReduxForm from '../../shared/components/ReduxForm-shared';

const footerStyled = `
  margin: 30px 0;
`;

export default class Project extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  componentWillMount() {
    const { project, setFormFields } = this.props;

    setFormFields({
      projectForm: {
        name: { value: project.name },
        endpoint: { value: project.endpoint },
        authorization: { value: project.authorization }
      }
    });
  }

  render() {
    const { forms, onSubmit, validation } = this.props;

    return (
      <ContentLayout className="Project-settings-container" heading="Project">
        <ReduxForm
          className="Project-settings-form"
          name="projectForm"
          onSubmit={onSubmit}
          validation={validation}
        >
          <FormRow className="Project-settings-form-row">
            <FormLabel className="Project-settings-form-label" htmlFor="name">
              Project Name
            </FormLabel>
            <Input
              name="name"
              value={forms.projectForm.fields.name.value}
              widths="large"
            />
            <FormError
              className="Project-settings-form-error"
              display={forms.projectForm.fields.name.error ? 'block' : 'none'}
            >
              {forms.projectForm.fields.name.error}
            </FormError>
          </FormRow>
          <FormRow className="Project-settings-form-row">
            <FormLabel
              className="Project-settings-form-label"
              htmlFor="endpoint"
            >
              Endpoint
            </FormLabel>
            <Input
              name="endpoint"
              value={forms.projectForm.fields.endpoint.value}
              widths="large"
            />
            <FormError
              className="Project-settings-form-error"
              display={
                forms.projectForm.fields.endpoint.error ? 'block' : 'none'
              }
            >
              {forms.projectForm.fields.endpoint.error}
            </FormError>
          </FormRow>
          <FormRow className="Project-settings-form-row">
            <FormLabel className="Project-settings-form-label" htmlFor="auth">
              Authorization (optional)
            </FormLabel>
            <Input
              name="auth"
              value={forms.projectForm.fields.auth.value}
              widths="large"
            />
            <FormError
              className="Project-settings-form-error"
              display={forms.projectForm.fields.auth.error ? 'block' : 'none'}
            >
              {forms.projectForm.fields.auth.error}
            </FormError>
          </FormRow>
          <FormRow
            className="Project-settings-form-row"
            styledFormRow={footerStyled}
          >
            <Button type="submit" className="Project-settings-save-button">
              Save Project
            </Button>
          </FormRow>
        </ReduxForm>
      </ContentLayout>
    );
  }
}
