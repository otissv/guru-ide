import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import ContentLayout from '../../shared/components/Content-layout-shared';
import { renderRoutes } from 'react-router-config';

export default class Schemas extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  handelOnSettingsSidebarClick({ route }) {
    const { setUiSettingsProps } = this.props;

    setUiSettingsProps({ route: route });
  }

  render() {
    const { route: { routes }, uiSettings: { route } } = this.props;

    return (
      <ContentLayout
        {...this.props}
        className="Settings-container"
        routes={routes}
        active={route}
        sidebar={{ items: routes, onClick: this.handelOnSettingsSidebarClick }}
        renderRoutes={renderRoutes(routes, { ...this.props })}
      />
    );
  }
}
