import React from 'react';
import styled from 'styled-components';
import autobind from 'class-autobind';
import Button from '../../../styled/components/Button';
import Checkbox from '../../../styled/components/Checkbox';
import ContentLayout from '../../shared/components/Content-layout-shared';
import FormRow from '../../../styled/components/form/FormRow';
import ReduxForm from '../../shared/components/ReduxForm-shared';

const SubHeading = styled.h2`
  font-size: 16px;
  font-weight: normal;
  margin: 0;
  margin-bottom: ${props => props.theme.spacing.small};
  padding-top: ${props => props.theme.spacing.small};

  ${props => props.styledSubheading};
`;

const styledSubheadingFirst = `
  padding: 0;
`;

const styledLabel = `
  display: block
  `;

const footerStyled = `
  margin: 30px 0;
`;

export default class Settings extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  render() {
    const { forms, handleClickSaveSettings } = this.props;

    return (
      <ContentLayout className="History-settings-container" heading="History">
        <ReduxForm name="settingsForm" onSubmit={handleClickSaveSettings}>
          <SubHeading styledSubheading={styledSubheadingFirst}>
            Request
          </SubHeading>

          <Checkbox
            name="clearRequestCollection"
            styledLabel={styledLabel}
            checked={forms.settingsForm.fields.clearRequestCollection.value}
          >
            Clear request collections
          </Checkbox>
          <Checkbox
            name="clearRequestHistory"
            styledLabel={styledLabel}
            checked={forms.settingsForm.fields.clearRequestHistory.value}
          >
            Clear request history
          </Checkbox>

          <SubHeading>Persisted</SubHeading>
          <Checkbox
            name="clearPersistedCollection"
            styledLabel={styledLabel}
            checked={forms.settingsForm.fields.clearPersistedCollection.value}
          >
            Clear persisted collections
          </Checkbox>

          <Checkbox
            name="clearPersistedHistory"
            styledLabel={styledLabel}
            checked={forms.settingsForm.fields.clearPersistedHistory.value}
          >
            Clear persisted history
          </Checkbox>

          <FormRow
            className="Project-settings-form-row"
            styledFormRow={footerStyled}
          >
            <Button primary type="submit">
              Clear History
            </Button>
          </FormRow>
        </ReduxForm>
      </ContentLayout>
    );
  }
}
