import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Button from '../../../styled/components/Button';

const ButtonStyled = styled(Button)`
  padding: 0 5px;
  height: 20px;
  font-size: 13px;
  line-height: 12px;
  text-transform: initial;
  margin-bottom: 2px;
  ${props => props.styledStatusButton};
`;

export default class StatusButton extends PureComponent {
  render() {
    const { children, onClick, styledStatusButton } = this.props;
    return (
      <ButtonStyled onClick={onClick} styledStatusButton={styledStatusButton}>
        {children}
      </ButtonStyled>
    );
  }
}
