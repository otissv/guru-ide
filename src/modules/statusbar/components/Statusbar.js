import React, { PureComponent } from 'react';
import styled from 'styled-components';
import StatusbarContainer from '../containers/container-statusbar';


const Container = styled.div`
  position: relative;
  z-index: 9999;
  background: ${props => props.theme.statusbar.background};
  bottom: 0;
  display: flex;
  font-size: ${props => props.theme.statusbar.fontSize};
  height: ${props => props.theme.statusbar.height};
  position: fixed;
  width: ${props => props.theme.statusbar.width};
`;

const BarInner = styled.div`
  flex: 1;
  display: flex;
  justify-content: flex-end;
  padding-right: 10px;
`;

const Error = styled.span`color: ${props => props.theme.statusbar.error};`;

const ConnectionStatus = styled.div`
  font-size: ${props => props.theme.statusbar.fontSize};
  color: ${props => props.theme.statusbar.fontColor};
`;

class Statusbar extends PureComponent {
  render() {
    const { isConnected } = this.props;

    return (
      <Container className="Statusbar-container">
        <BarInner className="Statusbar-inner">
          <ConnectionStatus className="Statusbar-connection">
            {isConnected ? null : (
              <Error className="GraphQL-connected">Schema not found</Error>
            )}
          </ConnectionStatus>
        </BarInner>
      </Container>
    );
  }
}

export default props => <StatusbarContainer component={Statusbar} />;
