import React from 'react';
import autobind from 'class-autobind';
import { connect } from '../../../store';
import getClassMethods from '../../../helpers/get-class-methods';

class StatusbarContainer extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);

    this.state = {
      showProjectDialog: false
    };
  }

  componentWillMount() {
    const { getSchemas, setSchemas } = this.props;

    getSchemas()
      .payload.then(response => setSchemas({ schemas: response.data.schema }))
      .catch(error => console.error(error));
  }

  handleClickSave() {}

  handleOnProjectDialogClickCancel(event) {
    event.preventDefault();
    this.setState({ showProjectDialog: false });
  }

  toggleProject() {
    const { setUiProjects, uiProjects } = this.props;

    setUiProjects({ show: !uiProjects.show });
    this.setState({ showProjectDialog: !this.state.showProjectDialog });
  }

  toggleMenu() {
    const { setUiNavProps, uiNavigation } = this.props;
    setUiNavProps({ show: !uiNavigation.show });
  }

  validation() {}

  render() {
    const Component = this.props.component;

    return (
      <Component
        {...getClassMethods(this)}
        showProjectDialog={this.state.showProjectDialog}
      />
    );
  }
}

export default connect(StatusbarContainer);
