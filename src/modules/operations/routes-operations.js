import React from 'react';
import Operations from './components/Operations';
import OperationsContainer from './containers/container-operations';
import { ROUTE_OPERATIONS } from '../router/route-constants';

export default {
  [ROUTE_OPERATIONS]: {
    component: props => (
      <OperationsContainer {...props} component={Operations} />
    )
  }
};
