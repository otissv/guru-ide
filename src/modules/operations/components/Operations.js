import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import ContentLayout from '../../shared/components/Content-layout-shared';
import List from '../../../styled/components/list/List';
import ListItem from '../../../styled/components/list/ListItem';
import ListItemNavLink from '../../../styled/components/list/ListItemNavLink';
import Svg from '../../../styled/components/Svg';
import Tabs from '../../../styled/components/tabs/Tabs';
import fileDownload from 'js-file-download';
import historyIcon from '../../../icons/back-in-time.svg';
import mutationIcon from '../../../icons/new-message.svg';
import queryIcon from '../../../icons/text-document.svg';
const Main = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

export default class Operations extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  handleOnDownloadClick(event) {
    const {
      activeOperationsTabOrder,
      projects,
      schemas,
      project: { id }
    } = this.props;

    const fileName = projects[id].name
      .replace(' ', '_')
      .concat(`_${activeOperationsTabOrder[0]}`)
      .toLowerCase();

    const schema = schemas[this.state.id];
    const data =
      typeof schema === 'string' ? schema : JSON.stringify(schema, null, 2);

    fileDownload(data, fileName);
  }

  createRoutes() {
    const { operations } = this.props;

    return operations
      ? [
          ...operations.Query.map(op => ({
            path: `/operations/${op.name}`,
            label: op.name,
            type: 'Query',
            icon: queryIcon
          })),
          ...operations.Mutation.map(op => ({
            path: `/operations/${op.name}`,
            label: op.name,
            type: 'Mutation',
            icon: mutationIcon
          }))
        ].filter(item => !item.label.match('Resolve'))
      : [];
  }

  getLeftSidebarItem() {
    const { activeOperationsTabOrder } = this.props;
    const active = activeOperationsTabOrder[0];
    const routes = this.createRoutes();

    return routes.filter(route => route.label).map(item => {
      return (
        <ListItem key={item.path}>
          <ListItemNavLink
            to={item.path}
            data-route={item.path}
            data-active={item.path === active}
            onClick={this.handleOnSidebarListItemClick}
            data-type={item.type}
          >
            {item.icon && (
              <Svg small styledSvg="margin-right: 5px" src={item.icon} />
            )}{' '}
            {item.label}
          </ListItemNavLink>
        </ListItem>
      );
    });
  }

  handleOnSidebarListItemClick(event) {
    const { handleOnSidebarListItemClick } = this.props;
    handleOnSidebarListItemClick({ ...event.target.dataset });
  }

  render() {
    const {
      activeOperationsTabOrder,
      handleOnCloseClick,
      handleOnTabChange,
      tabItems,
      handleOnSidebarButtonClick,
      uiOperations: { sidebarOperationsContent, route }
    } = this.props;

    const active = activeOperationsTabOrder[0];

    return (
      <ContentLayout
        className="Operations"
        styledContentLayout="margin-top: 10px"
        active={route}
        sidebar={{
          active: sidebarOperationsContent,
          buttonLeft: {
            component: (
              <List styledList="padding-top: 0;">
                {this.getLeftSidebarItem()}
              </List>
            ),
            icon: queryIcon,
            onClick: handleOnSidebarButtonClick,
            title: 'Operations',
            type: 'left'
          },
          buttonRight: {
            component: <div>History</div>,
            icon: historyIcon,
            onClick: handleOnSidebarButtonClick,
            title: 'History',
            type: 'right'
          }
        }}
      >
        <Main className="Operations-main">
          <Content className="Operations-content">
            <Tabs
              items={tabItems()}
              active={active}
              onChange={handleOnTabChange}
              handleOnCloseClick={handleOnCloseClick}
            />
          </Content>
        </Main>
      </ContentLayout>
    );
  }
}
