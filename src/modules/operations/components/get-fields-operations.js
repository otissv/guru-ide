import React from 'react';
import Checkbox from '../../../styled/components/Checkbox';

export default function getFields(props) {
  const { active, handleOnFieldsChange, operationTabs } = props;
  const { fields, query } = operationTabs[active] || {
    fields: [],
    query: { fields: [] }
  };

  return fields.map(field => {
    return (
      <Checkbox
        key={field.name}
        data-field={field.name}
        data-operation={active}
        styledLabel="display: block;"
        onChange={handleOnFieldsChange}
        checked={query.fields.filter(q => q.name === field.name)[0]}
      >
        {field.name}
      </Checkbox>
    );
  });
}
