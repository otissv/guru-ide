import React from 'react';
import styled from 'styled-components';
import Checkbox from '../../../styled/components/Checkbox';
import Input from '../../../styled/components/Input';

const Label = styled.label`
  display: block;
  margin-bottom: ${props => props.theme.spacing.xsmall};
`;

const LabelType = styled.span`color: ${props => props.theme.colors.success};`;

const ArgumentItems = styled.div``;

export default function getArguments(props) {
  const { active, onChange, operationTabs } = props;
  const args =
    (operationTabs &&
      operationTabs[active] &&
      operationTabs[active].operations[0].arguments) ||
    [];

  const getValue = name => {
    const value =
      (operationTabs &&
        operationTabs[active] &&
        operationTabs[active].query &&
        operationTabs[active].query.args &&
        operationTabs[active].query.args[name]) ||
      '';
    return value;
  };

  const argsInput = props => {
    return (
      <div>
        <Label>
          {props.name}{' '}
          <LabelType>
            {props.kind === 'ListType' ? `[${props.type}]` : props.type}
          </LabelType>
        </Label>
        <Input
          widths="large"
          data-operation={active}
          data-arg={props.name}
          onChange={onChange}
          value={getValue(props.name)}
          styledInput="margin-bottom: 20px;"
        />
      </div>
    );
  };

  const argsCheckbox = props => (
    <div>
      <Checkbox
        data-arg={props.name}
        data-operation={active}
        onChange={onChange}
        value={getValue(props.name)}
        styledCheckbox="margin-bottom: 20px;"
      />
    </div>
  );

  const type = {
    String: props => argsInput(props),
    Boolean: props => argsCheckbox(props),
    Int: props => argsInput(props)
  };

  return args.map(arg => {
    return (
      <ArgumentItems key={arg.name}>{type[arg.type]({ ...arg })}</ArgumentItems>
    );
  });
}
