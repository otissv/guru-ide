import React, { PureComponent } from 'react';
import styled from 'styled-components';
import EditorMain from './Editor-operations-main';

const Editor = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: ${props => props.theme.spacing.large};
`;

export default class OperationsEditor extends PureComponent {
  render() {
    return (
      <Editor className="Operations-editor">
        <EditorMain {...this.props} />
      </Editor>
    );
  }
}
