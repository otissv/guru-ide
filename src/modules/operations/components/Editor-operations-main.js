import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';

import Toolbar from '../../../styled/components/toolbar/Toolbar';
import ToolbarItem from '../../../styled/components/toolbar/Toolbar-item';
import EditorResult from '../../shared/components/editor/Editor-result-shared';
import IconButton from '../../../styled/components/IconButton';
import runIcon from '../../../icons/controller-play.svg';
import refreshIcon from '../../../icons/ccw.svg';
import saveIcon from '../../../icons/save.svg';
import infoIcon from '../../../icons/info.svg';
import Download from '../../shared/components/Download-shared';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: calc(100vh - 166px);
`;

const Main = styled.div`
  display: flex;
  flex: 1;
`;

const Aside = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: ${props => props.theme.spacing.large};
  overflow: auto;
  min-width: 280px;
`;

const Title = styled.h3`margin-top: ${props => props.theme.spacing.large};`;

const Fields = styled.div``;
const FieldsContainer = styled.div`flex: 1;`;

const VariablesContainer = styled.div`flex: 1;`;

const Variables = styled.div``;

const Result = styled.div`
  border-left: ${props => props.theme.border.thinSecondary};
  width: 100%;
  padding-left: ${props => props.theme.spacing.small};
  display: flex;
  flex-direction: column;
`;

export default class OperationsEditorMain extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  show(arr, component) {
    return arr.length > 0 ? component : null;
  }

  render() {
    const {
      active,
      args,
      fields,
      fetcher,
      handleOnResetClick,
      toggleInfoModel,
      toggleSaveModel,
      operationTabs,
      title
    } = this.props;

    const results = operationTabs[active].results
      ? JSON.stringify(operationTabs[active].results, null, 2)
      : '';

    const download = operationTabs[active].request;

    return (
      <Container>
        {this.show(
          fields,
          <Toolbar
            className="Operations-editor-toolbar"
            styledToolbar="margin-top: 10px"
          >
            <ToolbarItem className="Operations-editor-toolbar-item">
              <IconButton
                className="Operations-editor-toolbar-button"
                title="Execute Persisted"
                onClick={fetcher}
                src={runIcon}
              />
            </ToolbarItem>
            <ToolbarItem className="Operations-editor-toolbar-item">
              <IconButton
                className="Operations-editor-toolbar-icon-button"
                title="Save Persisted"
                src={saveIcon}
                onClick={toggleSaveModel}
              />
            </ToolbarItem>
            <ToolbarItem className="Operations-editor-toolbar-item">
              <IconButton
                className="Operations-editor-toolbar-icon-button"
                title="Reset Persisted"
                src={refreshIcon}
                onClick={handleOnResetClick}
              />
            </ToolbarItem>
          </Toolbar>
        )}

        <Main className="Operations-editor-main">
          <Aside className="Operations-editor-aside">
            {this.show(
              fields,
              <FieldsContainer className="Operations-editor-field-container">
                <Title className="Operations-editor-field-tilte">Fields</Title>
                <Fields className="Operations-editor-field-list">
                  {fields}
                </Fields>
              </FieldsContainer>
            )}
            {this.show(
              args,
              <VariablesContainer className="Operations-editor-variables-container">
                <Title className="Operations-editor-variables-title">
                  Variables
                </Title>
                <Variables className="Operations-editor-variables-list">
                  {args}
                </Variables>
              </VariablesContainer>
            )}
            <Download data={download} fileName={`query-${active}.graphql`}>
              {title} Download
            </Download>
          </Aside>

          {this.show(
            fields,
            <Result className="Operations-editor-results">
              <Title lassName="Operations-editor-results-title">Results</Title>
              <EditorResult
                lassName="Operations-editor-results-editor"
                results={results}
                handleOnChange={() => {}}
                active={active}
              />
            </Result>
          )}
        </Main>
      </Container>
    );
  }
}
