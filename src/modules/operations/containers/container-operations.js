import React from 'react';
import autobind from 'class-autobind';
import { connect } from '../../../store';
import { makeQuery } from '../../../helpers/async-query';
import getClassMethods from '../../../helpers/get-class-methods';

import OperationsEditor from '../components/Editor-operations';
import getFields from '../components/get-fields-operations';
import getArguments from '../components/get-args-operation';
import queryIcon from '../../../icons/text-document.svg';
import mutationIcon from '../../../icons/new-message.svg';

class OperationsContainer extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);
  }

  componentWillMount() {
    const { getSchemas, setSchemas } = this.props;

    getSchemas()
      .payload.then(response => setSchemas({ schemas: response.data.schema }))
      .catch(error => console.error(error));
  }

  args() {
    const { operationTabs, activeOperationsTabOrder } = this.props;
    const active = activeOperationsTabOrder[0];

    return getArguments({
      active,
      onChange: this.handleOnArgsChange,
      operationTabs
    });
  }

  fields() {
    const { operationTabs, activeOperationsTabOrder } = this.props;
    const active = activeOperationsTabOrder[0];

    return getFields({
      active,
      handleOnFieldsChange: this.handleOnFieldsChange,
      operationTabs
    });
  }

  fetcher(event) {
    const {
      addOperationTabRequestProps,
      addOperationTabResults,

      activeOperationsTabOrder,
      project: { endpoint },
      fetchQuery,
      operationTabs,
      uiOperations: { operationType }
    } = this.props;

    const active = activeOperationsTabOrder[0];

    const operationFields = operationTabs[active].query.fields;
    const operationArgs = operationTabs[active].query.args;
    const fields = operationTabs[active].fields || [];
    const args =
      operationTabs[active].operations.filter(o => o.name === active)[0]
        .arguments || [];

    const operation = {
      operation: active,
      args: Object.keys(operationArgs).reduce(
        (previous, key) => [
          ...previous,
          {
            ...args.filter(a => a.name === key)[0],
            value: operationArgs[key]
          }
        ],
        []
      ),
      fields: operationFields.map(
        o => fields.filter(f => f.name === o.name)[0]
      ),
      type: operationType.toLocaleLowerCase(),
      variables: operationArgs
    };

    if (operation.fields.length > 0) {
      addOperationTabRequestProps({
        operation: active,
        prop: makeQuery(operation)
      });

      fetchQuery({
        url: endpoint,
        operation
      }).payload.then(response => {
        addOperationTabResults({
          operation: active,
          prop: response.data
        });
      });
    }
  }

  getOperations(operation) {
    const { schemas } = this.props;

    const operations =
      (schemas.definition && schemas.definition.operations) || [];

    return operations;
  }

  getSidebarType() {
    const { uiOperations: { sidebarOperationsContent } } = this.props;
    return sidebarOperationsContent === 'left' ? 'Query' : 'Mutation';
  }

  handleOnArgsChange(event) {
    const { addOperationTabProps } = this.props;
    const arg = event.target.dataset.arg;
    const operation = event.target.dataset.operation;
    const value = event.target.value;

    addOperationTabProps({
      operation,
      query: { args: { name: arg, value } }
    });
  }

  handleOnCloseClick(id) {
    this.props.removeOperationTab({
      id,
      operationType: this.getSidebarType()
    });
  }

  handleOnFieldsChange(event) {
    const {
      activeOperationsTabOrder,
      addOperationTabProps,
      operationTabs,
      removeOperationTabProps
    } = this.props;

    const field = event.target.dataset.field;
    const operation = event.target.dataset.operation;
    const active = activeOperationsTabOrder[0];

    const operationTabField = operationTabs[active].query.fields.filter(
      f => f.name === field
    )[0];

    if (operationTabField) {
      removeOperationTabProps({
        field,
        operation
      });
    } else {
      addOperationTabProps({
        query: { fields: { name: field } },
        operation
      });
    }
  }

  handleOnPrettifyClick() {}

  handleOnResetClick() {
    const {
      activeOperationsTabOrder,
      addOperationTabs,
      operationTabs
    } = this.props;

    const active = activeOperationsTabOrder[0];
    const type = operationTabs[active].type;
    const data = this.createTabData({ active, type });

    addOperationTabs(data);
  }

  handleOnSidebarButtonClick(active) {
    const { setUiOperations } = this.props;

    setUiOperations({
      sidebarOperationsContent: active
    });
  }

  createTabData({ active, type }) {
    const { activeOperationsTabOrder, schemas } = this.props;

    const operationType = type;

    const typeName = schemas.definition.operations[operationType].filter(
      operation => operation.name === active
    )[0].type.name;

    const fields = schemas.definition.types.filter(
      type => type.name === typeName
    )[0].fields;

    const operations =
      (schemas.definition &&
        schemas.definition.operations &&
        schemas.definition.operations[operationType].filter(
          operation => operation.name === active
        )) ||
      {};

    return {
      [active]: {
        fields,
        operations,
        query: { fields: [], args: [] },
        request: '',
        type
      }
    };
  }

  handleOnSidebarListItemClick({ route, type }) {
    const {
      addOperationTabs,
      operationTabs,

      setActiveOperationsTab,
      setUiOperations
    } = this.props;

    const activeSplit = route.split('/');
    const active = activeSplit[activeSplit.length - 1];

    const data = this.createTabData({ active, type });

    const activeTabs = operationTabs[active];

    setActiveOperationsTab(active);

    setUiOperations({ route });

    if (!activeTabs) {
      addOperationTabs(data);
    }
  }

  handleOnTabChange({ kitid, route }) {
    const { setActiveOperationsTab, setUiOperations } = this.props;

    setActiveOperationsTab(kitid);
    setUiOperations({ route });
  }

  toggleSaveModel() {
    this.props.setSaveModal(true);
  }

  toggleInfoModel(bool) {
    const {
      activeOperationsTabOrder,
      operationTabs,
      setUiOperations,
      uiOperations: { isInfoModalOpen }
    } = this.props;

    const tab = operationTabs[activeOperationsTabOrder[0]];

    if (tab.request.trim() !== '')
      setUiOperations({ isInfoModalOpen: !isInfoModalOpen });
  }

  tabItems() {
    const {
      activeOperationsTabOrder,
      operationTabs,
      route: { routes },
      uiOperations: { gqlTheme, isInfoModalOpen, isSaveModalOpen }
    } = this.props;

    const active = activeOperationsTabOrder[0];
    let title;

    return (
      (operationTabs &&
        Object.keys(operationTabs).reduce((previous, operation) => {
          const type = operationTabs[operation].type;

          return {
            ...previous,
            [operation]: {
              component: {
                element: OperationsEditor,
                props: {
                  active,
                  args: this.args(),
                  editorTheme: gqlTheme,
                  fetcher: this.fetcher,
                  fields: this.fields(),
                  handleOnPrettifyClick: this.handleOnPrettifyClick,
                  handleOnResetClick: this.handleOnResetClick,
                  isInfoModalOpen,
                  isSaveModalOpen,
                  operationTabs,
                  operationType: type,
                  routes,
                  title,
                  toggleInfoModel: this.toggleInfoModel,
                  toggleSaveModel: this.toggleSaveModel
                }
              },
              icon: type === 'Query' ? queryIcon : mutationIcon,
              name: operation,
              id: operation,
              path: `/operations/${operation}`,
              handleOnCloseClick: () => {}
            }
          };
        }, {})) ||
      {}
    );
  }

  render() {
    const { component, schemas } = this.props;
    const Component = component;

    return (
      <Component
        {...getClassMethods(this)}
        operations={schemas.definition.operations}
      />
    );
  }
}

export default connect(OperationsContainer);
