import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import ProjectContainer from '../containers/container-projects';
import NewProject from './New-projects';
import logo from '../../../images/guru.svg';

const Container = styled.div`
  display: flex;
  padding: 5%;
  flex-direction: column;
`;

const LeftColumn = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const RightColumn = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-left: ${props => props.theme.spacing.large};
`;

const Heading = styled.h1`
  position: absolute;
  top: -100%;
`;

const SubHeading = styled.h2`
  font-size: ${props => props.theme.font.large};
  margin-top: 0;
`;

const SmallHeading = styled.h3`
  margin-bottom: 5px;
`;

const Logo = styled.img`
  display: block;
  font-size: 18px;
  margin-bottom: ${props => props.theme.spacing.xlarge};
  margin: -2px 10px 0 0;
  text-decoration: none;
  width: 150px;
`;

const Main = styled.div`
  display: flex;
`;

const ListProjects = styled.div`
  overflow: auto;
  max-width: 360px;
  max-height: 270px;
`;

const ListProjectItems = styled.div``;

const ProjectLink = styled.button`
  background: ${props => props.theme.colors.background};
  border-bottom: 1px solid ${props => props.theme.colors.background};
  border: none;
  color: ${props => props.theme.colors.primary};
  font-size: ${props => props.theme.font.small};
  outline: none;
  padding: 4px 1px;
  text-align: left;
  transition: ${props => 'border-bottom ' + props.theme.animation.easeMedium};

  &:hover {
    color: ${props => props.theme.colors.foreground};
    background: ${props => props.theme.colors.secondary};
  }

  ${props => props.styledProjectLink};
`;

// const ProjectLinkFileInput = styled.input`
//   position: relative;
//   font-size: ${props => props.theme.font.small};
//   outline: none;
//   max-width: 360px;

//   &:hover {
//     border-bottom: ${props => props.theme.border.thinPrimary};
//   }

//   &:after {
//     background: ${props => props.theme.colors.background};
//     border-bottom: 1px solid ${props => props.theme.colors.background};
//     border: none;
//     color: ${props => props.theme.colors.primary};
//     content: 'Config...';
//     display: inline-block;
//     font-size: ${props => props.theme.font.small};
//     left: 0;
//     line-height: 19px;
//     outline: none;
//     padding: 4px 1px;
//     position: absolute;
//     text-align: left;
//     top: 0px;
//     transition: border-bottom ease 0.2s;
//     width: 73px;
//   }

//   ${props => props.styledProjectLink};
// `;

const ProjectEndPoint = styled.span`
  color: ${props => props.theme.colors.foreground};
`;

const Leading = styled.p`
  margin-top: 0;
  margin-bottom: 30px;
`;

class Projects extends PureComponent {
  constructor() {
    super(...arguments);
    autobind(this);
  }

  handleOnRecentClick(event) {
    const id = event.currentTarget.dataset.kitid;
    this.props.handleOnRecentClick(id);
  }

  getProjects() {
    const projects = Array.isArray(this.props.projects)
      ? this.props.projects
      : [];

    return projects.map(project => (
      <ListProjectItems key={project.id}>
        <ProjectLink onClick={this.handleOnRecentClick} data-kitid={project.id}>
          {project.name}
          <ProjectEndPoint>{` ${project.endpoint}`}</ProjectEndPoint>
        </ProjectLink>
      </ListProjectItems>
    ));
  }

  render() {
    const { forms, handleOnNewProjectSubmit, validation } = this.props;

    return (
      <Container>
        <Heading>GraphQl GURU IDE</Heading>
        <Logo src={logo} alt="Guru" />
        <Leading>GraphQL GURU IDE</Leading>
        <Main>
          {this.props.projects.length !== 0 ? (
            <LeftColumn>
              <SubHeading>Projects</SubHeading>

              <SmallHeading>Recent</SmallHeading>
              <ListProjects>{this.getProjects()}</ListProjects>
            </LeftColumn>
          ) : null}

          <RightColumn>
            <NewProject
              forms={forms}
              onSubmit={handleOnNewProjectSubmit}
              validation={validation}
            />
          </RightColumn>
        </Main>
      </Container>
    );
  }
}

export default props => <ProjectContainer component={Projects} />;
