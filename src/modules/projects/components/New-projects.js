import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Input from '../../../styled/components/Input';
import FormRow from '../../../styled/components/form/FormRow';
import FormLabel from '../../../styled/components/form/FormLabel';
import FormError from '../../../styled/components/form/FormError';
import Button from '../../../styled/components/Button';
import ReduxForm from '../../shared/components/ReduxForm-shared';

const Container = styled.section`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Heading = styled.h2`
  margin-top: 0;
  font-size: ${props => props.theme.font.xlarge};
`;

const footerStyled = `
display: flex;
flex-direction: row;

align-items: flex-end;
margin-bottom: 0;
`;

export default class NewProject extends PureComponent {
  render() {
    const { forms, onSubmit, validation } = this.props;

    return (
      <Container>
        <Heading>New Project</Heading>
        <ReduxForm
          name="projectForm"
          onSubmit={onSubmit}
          validation={validation}
        >
          <FormRow>
            <FormLabel htmlFor="endpoint">Project Name</FormLabel>
            <Input
              widths="xlarge"
              name="name"
              value={forms.projectForm.fields.name.value}
            />
            <FormError
              display={forms.projectForm.fields.name.error ? 'block' : 'none'}
            >
              {forms.projectForm.fields.name.error}
            </FormError>
          </FormRow>

          {forms.projectForm.fields.name.value ? (
            <div>
              <FormRow>
                <FormLabel htmlFor="endpoint">Endpoint</FormLabel>
                <Input
                  widths="xlarge"
                  name="endpoint"
                  value={forms.projectForm.fields.endpoint.value}
                />
                <FormError
                  display={
                    forms.projectForm.fields.endpoint.error ? 'block' : 'none'
                  }
                >
                  {forms.projectForm.fields.endpoint.error}
                </FormError>
              </FormRow>

              <FormRow>
                <FormLabel htmlFor="auth">Authorization (optional)</FormLabel>
                <Input
                  widths="xlarge"
                  name="auth"
                  value={forms.projectForm.fields.auth.value}
                />
                <FormError
                  display={
                    forms.projectForm.fields.auth.error ? 'block' : 'none'
                  }
                >
                  {forms.projectForm.fields.auth.error}
                </FormError>
              </FormRow>

              <FormRow styledFormRow={footerStyled}>
                <Button primary type="submit">
                  Create Project
                </Button>
              </FormRow>
            </div>
          ) : null}
        </ReduxForm>
      </Container>
    );
  }
}
