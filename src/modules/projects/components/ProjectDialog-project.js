import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import StatusButton from '../../statusbar/components/StatusButton-statusbar';

import Input from '../../../styled/components/Input';
import FormRow from '../../../styled/components/form/FormRow';
import FormLabel from '../../../styled/components/form/FormLabel';
import FormError from '../../../styled/components/form/FormError';
import Button from '../../../styled/components/Button';
import ReduxForm from '../../shared/components/ReduxForm-shared';

const Container = styled.div`
  display: flex;
  text-align: right;
  flex-direction: column;
`;

const Overlay = styled.div`
  position: fixed;
  display: ${props => (props.show ? 'block' : 'none')};
  top: 0;
  left: 0;
  background: ${props => props.theme.colors.background};
  height: 100vh;
  width: 100%;
  opacity: 0.5;
`;

const ProjectContainer = styled.div`
  background: ${props => props.theme.colors.background};
  padding: ${props => props.theme.spacing.small};
  width: 300px;
  border: ${props => props.theme.border.thin};
  position: fixed;
  transform: ${props =>
    props.show ? 'translate(-221px,-310px);' : 'translate(-2260px, 20px);'};
`;

const footerStyled = `
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: flex-end;
  margin-bottom: 0;
`;

export default class ProjectDialog extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  componentWillMount() {
    const { project, setFormFields } = this.props;

    setFormFields({
      projectForm: {
        name: { value: project.name },
        endpoint: { value: project.endpoint },
        authorization: { value: project.authorization }
      }
    });
  }

  render() {
    const {
      forms,
      project,
      onClickSave,
      onCancelClick,
      validation,
      onClick,
      show
    } = this.props;

    return (
      <Container>
        <StatusButton onClick={onClick}>{project.name}</StatusButton>
        <Overlay show={show} onClick={onCancelClick} />
        <ProjectContainer show={show}>
          <ReduxForm
            name="projectForm"
            onSubmit={onClickSave}
            validation={validation}
          >
            <FormRow>
              <FormLabel htmlFor="name">Project Name</FormLabel>
              <Input name="name" value={forms.projectForm.fields.name.value} />
              <FormError
                display={forms.projectForm.fields.name.error ? 'block' : 'none'}
              >
                {forms.projectForm.fields.name.error}
              </FormError>
            </FormRow>
            <FormRow>
              <FormLabel htmlFor="endpoint">Endpoint</FormLabel>
              <Input
                name="endpoint"
                value={forms.projectForm.fields.endpoint.value}
              />
              <FormError
                display={
                  forms.projectForm.fields.endpoint.error ? 'block' : 'none'
                }
              >
                {forms.projectForm.fields.endpoint.error}
              </FormError>
            </FormRow>
            <FormRow>
              <FormLabel htmlFor="auth">Authorization (optional)</FormLabel>
              <Input name="auth" value={forms.projectForm.fields.auth.value} />
              <FormError
                display={forms.projectForm.fields.auth.error ? 'block' : 'none'}
              >
                {forms.projectForm.fields.auth.error}
              </FormError>
            </FormRow>
            <FormRow styledFormRow={footerStyled}>
              <Button
                styledButton="margin-right: 10px;"
                onClick={onCancelClick}
              >
                Cancel
              </Button>

              <Button primary type="submit">
                Save
              </Button>
            </FormRow>
          </ReduxForm>
        </ProjectContainer>
      </Container>
    );
  }
}
