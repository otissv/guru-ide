import React from 'react';
import autobind from 'class-autobind';
import { connect } from '../../../store';
import getClassMethods from '../../../helpers/get-class-methods';
import cuid from 'cuid';

class ProjectContainer extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);
  }

  componentDidMount() {
    const { getProjects, setProjects } = this.props;

    getProjects()
      .payload.then(response => {
        setProjects({ projects: response.data.ideProjectFindAll });
      })
      .catch(error => console.error(error));
  }

  handleOnRecentClick(id) {
    const { updateProject, projects } = this.props;

    updateProject(projects.filter(p => p.id === id)[0]);
  }

  handleOnNewProjectSubmit() {
    const { createProject, forms: { projectForm }, updateProject } = this.props;
    const fields = projectForm.fields;

    const values = Object.keys(fields).reduce(
      (previous, key) => ({ ...previous, [key]: fields[key].value }),
      { id: cuid() }
    );

    createProject(values)
      .payload.then(response => {
        updateProject(values);
      })
      .catch(error => console.error(error));
  }

  render() {
    const Component = this.props.component;

    return <Component {...getClassMethods(this)} />;
  }
}

export default connect(ProjectContainer);
