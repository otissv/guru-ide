import React from 'react';
import autobind from 'class-autobind';
import { connect } from '../../../store';
import getClassMethods from '../../../helpers/get-class-methods';
// import axios from 'axios';
import fetch from 'isomorphic-fetch';

class SchemasContainer extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);
  }

  componentWillMount() {
    const { getSchemas, setSchemas } = this.props;

    getSchemas()
      .payload.then(response => setSchemas({ schemas: response.data.schema }))
      .catch(error => console.error(error));
  }

  introspectionProvider(query) {
    const { endpoint } = this.props;

    // return axios({
    //   method: 'post',
    //   url: endpoint,
    //   headers: { 'Content-Type': 'application/json' },
    //   data: JSON.stringify({ query })
    // })
    //   .then(response => response.data)
    //   .catch(error => console.error(error));
    return fetch(endpoint, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query: query })
    }).then(response => response.json());
  }

  render() {
    const Component = this.props.component;

    return <Component {...getClassMethods(this)} />;
  }
}

export default connect(SchemasContainer);
