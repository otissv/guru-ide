import React, { PureComponent } from 'react';
import { Voyager } from 'graphql-voyager';

export default class GqlVoyager extends PureComponent {
  render() {
    console.log(this.props.introspectionProvider);
    return <Voyager introspection={this.props.introspectionProvider} />;
  }
}
