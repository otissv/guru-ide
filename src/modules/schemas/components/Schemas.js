import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import ContentLayout from '../../shared/components/Content-layout-shared';
import IconButton from '../../../styled/components/IconButton';
import downloadIcon from '../../../icons/download.svg';
import fileDownload from 'js-file-download';
import { renderRoutes } from 'react-router-config';

export default class Schemas extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  handleOnDownloadClick(event) {
    const { projects, schemas, project: { id } } = this.props;
    const fileName = projects[id].name
      .replace(' ', '_')
      .concat(`_${this.state.active}`)
      .toLowerCase();

    const schema = schemas[this.state.id];
    const data =
      typeof schema === 'string' ? schema : JSON.stringify(schema, null, 2);

    fileDownload(data, fileName);
  }

  handelOnSettingsSidebarItemClick({ route }) {
    this.props.setUiSchemas({ route });
  }

  render() {
    const { route: { routes }, uiSchemas: { route } } = this.props;

    return (
      <ContentLayout
        className="Schemas"
        routes={routes}
        active={route}
        sidebar={{
          onClick: this.handelOnSettingsSidebarItemClick
        }}
        renderRoutes={renderRoutes(routes, { ...this.props })}
      >
        <IconButton src={downloadIcon} onClick={this.handleOnDownloadClick} />
      </ContentLayout>
    );
  }
}
