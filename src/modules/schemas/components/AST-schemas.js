import React, { PureComponent } from 'react';
import Tree from './Tree-schemas';

export default class AST extends PureComponent {
  render() {
    return <Tree data={this.props.schemas.ast} />;
  }
}
