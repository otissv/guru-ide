import React, { PureComponent } from 'react';
import Tree from './Tree-schemas';

export default class Definition extends PureComponent {
  render() {
    return <Tree data={this.props.schemas.definition} />;
  }
}
