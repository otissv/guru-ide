import React from 'react';
import Schemas from './components/Schemas';
import SchemasContainer from './containers/container-schemas';
import AST from './components/AST-schemas';
import Definition from './components/Definition-schemas';
import ViewSchema from './components/View-schemas';

import {
  ROUTE_SCHEMAS,
  ROUTE_SCHEMAS_AST,
  ROUTE_SCHEMAS_DEFINITION,
  ROUTE_SCHEMAS_DSL
} from '../router/route-constants';

export default {
  [ROUTE_SCHEMAS]: {
    component: props => <SchemasContainer {...props} component={Schemas} />,
    routes: [
      {
        path: ROUTE_SCHEMAS_AST,
        label: 'AST',
        component: AST
      },
      {
        path: ROUTE_SCHEMAS_DEFINITION,
        label: 'Definition',
        component: Definition
      },
      {
        path: ROUTE_SCHEMAS_DSL,
        label: 'DSL',
        component: props => (
          <ViewSchema {...props} schema={props.schemas.print} />
        )
      }
    ]
  }
};
