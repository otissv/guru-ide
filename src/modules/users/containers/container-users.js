import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import { connect } from '../../../store';
import getClassMethods from '../../../helpers/get-class-methods';
import Users from '../components/Users';

class UsersContainer extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);
  }

  render() {
    return <Users {...getClassMethods(this)} />;
  }
}

export default connect(UsersContainer);
