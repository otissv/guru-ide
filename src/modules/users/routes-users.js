import React from 'react';
import UsersContainer from './containers/container-users';
import Users from './components/Users';
import EditUser from './components/Edit-users';
import ListUser from './components/List-users';
import NewUser from './components/New-users';
import ShowUser from './components/Show-users';

import {
  ROUTE_USERS,
  ROUTE_USERS_EDIT,
  ROUTE_USERS_NEW,
  ROUTE_USERS_SHOW
} from '../router/route-constants';

export default {
  [ROUTE_USERS]: {
    component: props => <UsersContainer {...props} component={Users} />,
    routes: [
      {
        label: 'Edit User',
        path: ROUTE_USERS_EDIT,
        component: EditUser,
        active: true
      },
      {
        label: 'New User',
        path: ROUTE_USERS_NEW,
        component: NewUser
      },
      {
        label: 'Show User',
        path: ROUTE_USERS_SHOW,
        component: ShowUser
      }
    ]
  }
};
