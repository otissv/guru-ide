import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import Button from '../../../styled/components/Button';
import ContentLayout from '../../shared/components/Content-layout-shared';
import FormError from '../../../styled/components/form/FormError';
import FormLabel from '../../../styled/components/form/FormLabel';
import FormRow from '../../../styled/components/form/FormRow';
import Input from '../../../styled/components/Input';
import ReduxForm from '../../shared/components/ReduxForm-shared';

const footerStyled = `
  margin: 30px 0;
`;

export default class EditUser extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  render() {
    const { forms, onSubmit, validation } = this.props;

    return (
      <ContentLayout className="User-prot" heading="Project">
        <ReduxForm
          className="Project-settings-form"
          name="projectForm"
          onSubmit={onSubmit}
          validation={validation}
        >
          <FormRow className="Project-settings-form-row">
            <FormLabel className="Project-settings-form-label" htmlFor="name">
              Project Name
            </FormLabel>
            <Input
              name="name"
              value={forms.projectForm.fields.name.value}
              widths="large"
            />
          </FormRow>
          <FormRow
            className="Project-settings-form-row"
            styledFormRow={footerStyled}
          >
            <Button type="submit" className="Project-settings-save-button">
              Save Project
            </Button>
          </FormRow>
        </ReduxForm>
      </ContentLayout>
    );
  }
}
