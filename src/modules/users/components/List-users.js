import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import ContentLayout from '../../shared/components/Content-layout-shared';

export default class ListUsers extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  render() {
    const { forms, onSubmit, validation } = this.props;

    return <ContentLayout className="User-profile" heading="User profile" />;
  }
}
