// Navigation menu
export const ROUTE_OPERATIONS = '/operations';
export const ROUTE_PERSISTED = '/persisted';
export const ROUTE_REQUEST = '/request';
export const ROUTE_SCHEMAS = '/schemas';
export const ROUTE_SETTINGS = '/settings';

// schemas/
export const ROUTE_SCHEMAS_AST = '/schemas/ast';
export const ROUTE_SCHEMAS_DEFINITION = '/schemas/definition';
export const ROUTE_SCHEMAS_DSL = '/schemas/dsl';

// settings/
export const ROUTE_SETTINGS_DATABASE = '/settings/databases';
export const ROUTE_SETTINGS_DATABASE_JSONDB = '/settings/databases/jsondb';
export const ROUTE_SETTINGS_DATABASE_MONGODB = '/settings/databases/mongodb';
export const ROUTE_SETTINGS_DATABASE_MSSQL = '/settings/databases/mssql';
export const ROUTE_SETTINGS_DATABASE_MYSQL = '/settings/databases/mysql';
export const ROUTE_SETTINGS_DATABASE_POSTGRESQL =
  '/settings/databases/postgresql';
export const ROUTE_SETTINGS_DATABASE_REDIS = '/settings/databases/redis';
export const ROUTE_SETTINGS_DATABASE_RETHINKDB =
  '/settings/databases/rethinkdb';
export const ROUTE_SETTINGS_DATABASE_SQLITE = '/settings/databases/sqlite';
export const ROUTE_SETTINGS_ENVIRONMENT = '/settings/environment';
export const ROUTE_SETTINGS_HISTORY = '/settings/history';
export const ROUTE_SETTINGS_PROJECT = '/settings/project';

// users
export const ROUTE_USERS = '/users';
export const ROUTE_USERS_EDIT = '/users/edit/:id';
export const ROUTE_USERS_NEW = '/users/new';
export const ROUTE_USERS_SHOW = '/users/:id';

// export const ROUTE_ = '/'
// export const ROUTE_ = '/'
// export const ROUTE_ = '/'
// export const ROUTE_ = '/'
// export const ROUTE_ = '/'
// export const ROUTE_ = '/'
// export const ROUTE_ = '/'
