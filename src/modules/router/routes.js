import LayoutView from '../../core/app/views/layout-view-app';
import operation from '../operations/routes-operations';
import persisted from '../persisted/routes-persisted';
import request from '../request/routes-request';
import schemas from '../schemas/routes-schemas';
import settings from '../settings/routes-settings';
import users from '../users/routes-users';

import {
  ROUTE_OPERATIONS,
  ROUTE_REQUEST,
  ROUTE_PERSISTED,
  ROUTE_SCHEMAS,
  ROUTE_SETTINGS,
  ROUTE_USERS
} from './route-constants';

export default [
  {
    component: LayoutView,
    routes: [
      {
        path: ROUTE_OPERATIONS,
        label: 'operations',
        ...operation[ROUTE_OPERATIONS]
      },
      {
        path: ROUTE_PERSISTED,
        label: 'persisted',
        ...persisted[ROUTE_PERSISTED]
      },
      {
        path: ROUTE_REQUEST,
        label: 'request',
        ...request[ROUTE_REQUEST]
      },
      {
        path: ROUTE_SCHEMAS,
        label: 'schemas',
        ...schemas[ROUTE_SCHEMAS]
      },
      {
        path: ROUTE_SETTINGS,
        label: 'settings',
        ...settings[ROUTE_SETTINGS],
        active: true
      },
      {
        path: ROUTE_USERS,
        label: 'users',
        ...users[ROUTE_USERS]
      }
    ]
  }
];
