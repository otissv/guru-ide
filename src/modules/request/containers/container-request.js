import React from 'react';
import axios from 'axios';
import autobind from 'class-autobind';
import styled from 'styled-components';
import { connect } from '../../../store';
import getClassMethods from '../../../helpers/get-class-methods';
import { parse, print } from 'graphql';
import cuid from 'cuid';
import RequestEditor from '../components/Editor-request';
import { TAB_RESET_STATE } from '../../shared/constants-shared';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

class RequestContainer extends React.Component {
  constructor() {
    super(...arguments);
    autobind(this);

    this.request = {
      history: {
        id: '',
        query: '',
        variables: ''
      },
      collections: {}
    };
  }

  componentWillMount() {
    const {
      createRequestCollections,
      createRequestHistory,
      getRequest,
      project: { id }
    } = this.props;

    // fetch queries and history from server
    getRequest(id)
      .payload.then(response => {
        if (response.data.ideRequestFindAll) {
          createRequestCollections(response.data.ideRequestFindAll);
        }

        if (response.data.ideRequestHistoryFind) {
          createRequestHistory(response.data.ideRequestHistoryFind);
        }
      })
      .catch(error => console.error(error));
  }

  fetcher(graphQLParams) {
    if (graphQLParams.query === '') {
      return Promise.resolve('Please provide a query.');
    } else {
      const {
        activeRequestTabOrder,
        addRequestHistoryItem,
        addRequestTabs,
        project,
        saveRequestHistory,
        requestTabs
      } = this.props;

      const active = activeRequestTabOrder[0];
      const endpoint = project.endpoint;

      // axios defaults
      axios.defaults.baseURL = endpoint;
      axios.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded';

      const axiosConfig = {
        url: endpoint,
        method: 'POST'
      };

      const startResponseTime = new Date().getTime();

      return axios({
        ...axiosConfig,
        data: graphQLParams
      })
        .then(response => {
          if (!response.data.data.__schema) {
            const date = Date.parse(new Date()).toString();
            const isHistory = active === 'history';
            const id = isHistory ? date : active;
            const name = requestTabs[active].name;

            const data = {
              id,
              endpoint,
              name,
              query: graphQLParams.query,
              results: {
                request: {
                  data: response.config.data,
                  headers: response.config.headers,
                  method: 'POST',
                  url: response.config.url,
                  xsrfCookieName: response.config.xsrfCookieName,
                  xsrfHeaderName: response.config.xsrfHeaderName
                },

                headers: response.headers,
                status: `${response.status} ${response.statusText}`,
                code: 200,
                time: `${new Date().getTime() - startResponseTime}`,
                response: response.data
              },
              variables: graphQLParams.variables
                ? JSON.stringify(graphQLParams.variables)
                : ''
            };

            addRequestHistoryItem({
              [date]: data
            });

            saveRequestHistory({
              ...data,
              id: date,
              projectId: project.id
            });

            addRequestTabs(data, isHistory);
            if (!isHistory) addRequestTabs(data);

            return response.data.data;
          } else {
            return '';
          }
        })
        .catch(error => {
          if (error.response) {
            console.error(error.response);
            // Response status code 2xx
            // setQueryResultProps({
            //   response: error.response.data,
            //   status: `${error.response.status} failed`,
            //   code: parseInt(error.response.code, 10),
            //   headers: error.response.headers
            // });
          } else if (error.request) {
            // No response was received
            console.error(error.request);
            // setQueryResultProps({
            //   response: error.response.data,
            //   status: '502 failed...',
            //   code: 502,
            //   headers: {}
            // });
          } else {
            console.error(error);
            // setQueryResultProps({
            //   response: 'Connection failed',
            //   status: '500 failed',
            //   code: 500,
            //   headers: {}
            // });
          }
        });
    }
  }

  getQuery(queryString) {
    const { activeRequestTabOrder, requestTabs } = this.props;

    const tab = requestTabs[activeRequestTabOrder[0]];

    const query = this.request.query || tab.query;

    if (query.trim() === '') {
      return '';
    } else {
      return this.prettyQuery(query);
    }
  }

  getSidebarType() {
    const { uiRequest: { sidebarRequestContent } } = this.props;
    return sidebarRequestContent === 'left' ? 'left' : 'right';
  }

  handleOnEditQuery(query) {
    this.setQuery(query);
  }

  handleOnEditVariables(variables) {
    this.request.variables = variables;
  }

  handleOnPrettifyClick(event) {
    const query = this.getQuery(this.request.query);
    this.setQuery(query);
  }

  handleOnHistoryRemoveClick(id) {
    const {
      deleteHistoryItem,
      removeHistoryItem,
      removeTab,
      project
    } = this.props;

    removeHistoryItem({ projectId: project.id, id })
      .payload.then(response => {
        removeTab(id);
        deleteHistoryItem(id);
      })
      .catch(error => console.error(error));
  }

  handleOnCloseClick(id) {
    const { removeRequestTab } = this.props;
    removeRequestTab(id);
  }

  handleRequestCollectionItemClick({ route, collection }) {
    const {
      addRequestTabs,
      setActiveRequestTab,
      requestCollection,
      setUiRequest
    } = this.props;

    const routeSplit = route.split('/');
    const id = routeSplit[routeSplit.length - 1];
    let query;
    let variables;

    if (
      this.request.collections[collection] &&
      this.request.collections[collection][id]
    ) {
      query = this.request.collections[collection][id].query;
      variables = this.request.collections[collection][id].variables;
    } else {
      query = requestCollection[collection][id].query;
      variables = requestCollection[collection][id].variables;

      this.request.collections = {
        ...this.request.collections,
        [collection]: {
          ...this.request.collections[collection],
          [id]: {
            id,
            query,
            variables
          }
        }
      };
    }

    const tab = {
      ...requestCollection[collection][id],
      query,
      variables
    };

    setActiveRequestTab(id);
    addRequestTabs(tab);
    setUiRequest({ route });
  }

  handleOnHistoryItemClick({ route }) {
    const {
      addRequestTabs,
      requestHistory,
      setActiveRequestTab,
      setUiRequest
    } = this.props;
    const id = route.split('/request/history/')[1];

    setActiveRequestTab('history');
    addRequestTabs(requestHistory[id], 'history');
    setUiRequest({ route });
  }

  handleOnResetClick() {
    const {
      activeRequestTabOrder,
      addRequestTabs,
      project,
      requestTabs,
      updateRequestCollection,
      resetForm
    } = this.props;
    const id = activeRequestTabOrder[0];

    this.request = {
      id: '',
      query: '',
      variables: ''
    };

    const isHistory = id === 'history' ? true : false;

    const tab = {
      ...requestTabs[id],
      ...TAB_RESET_STATE,
      endpoint: project.endpoint,
      isHistory
    };

    resetForm('saveForm');

    if (!isHistory) {
      addRequestTabs(tab, isHistory);
      updateRequestCollection(tab);
    }
  }

  handleOnSaveClick(values) {
    const {
      activeRequestTabOrder,
      requestTabs,
      addRequestCollection,
      createRequest,
      resetForm,
      project: { id },
      request,
      setSelectedRequest,
      setUiRequest
    } = this.props;
    const { collection, description, name } = values;
    const active = activeRequestTabOrder[0];
    const tab = requestTabs[active];

    const data = {
      ...tab,
      id: tab.id.trim() === '' || tab.isHistory ? cuid() : tab.id,
      results: JSON.stringify(tab.results)
    };

    setSelectedRequest(data);
    addRequestCollection(data);
    createRequest(data)
      .payload.then(response => {
        if (response.data.ideRequestCreate.RESULTS_.result === 'failed') {
        }
      })
      .catch(error => console.error(error));
    setUiRequest({ isSaveModalOpen: false });
    resetForm('saveForm');
  }

  handleOnTabChange({ kitid }) {
    this.props.setActiveRequestTab(kitid);
  }

  prettyQuery(query) {
    return print(parse(query));
  }

  removeRequestTab(id) {
    this.props.removeRequestTab(id);
  }

  setQuery(query) {
    const { activeRequestTabOrder, requestTabs, addRequestTabs } = this.props;
    const id = activeRequestTabOrder[0];
    const isHistory = id === 'history';

    const tab = {
      ...requestTabs[id],
      query
    };

    if (isHistory) {
      this.request.history.query = query;
    } else {
      if (this.request.collections[tab.collection]) {
        this.request.collections[tab.collection][id].query = query;
      } else {
        this.request.collections[tab.collection] = {
          id: '',
          query: '',
          variables: ''
        };
      }
    }

    addRequestTabs(tab, isHistory);
  }

  setSaveModal(bool) {
    const {
      activeRequestTabOrder,
      requestTabs,
      setQueryResultProps,
      setUiRequest,
      setSaveFormFields
    } = this.props;

    const activeId = activeRequestTabOrder[0];
    const activeTab = requestTabs[activeId];

    setSaveFormFields({
      name: { value: activeTab.name },
      collection: { value: activeTab.collection },
      description: { value: activeTab.description }
    });

    if (activeTab.query.trim() === '') {
      setQueryResultProps({ response: 'Please provide a query.' });
    } else {
      setUiRequest({ isSaveModalOpen: bool });
    }
  }

  tabItems() {
    const {
      activeRequestTabOrder,
      requestTabs,
      uiOperations: { gqlTheme }
    } = this.props;

    const active = activeRequestTabOrder[0];

    return (
      (requestTabs &&
        Object.keys(requestTabs).reduce((previous, id) => {
          return {
            ...previous,
            [id]: {
              handleOnCloseClick: () => {},
              id,
              name: id === 'history' ? 'history' : requestTabs[id].name,
              component: {
                element: RequestEditor,
                props: {
                  active,
                  editorTheme: gqlTheme,
                  fetcher: this.fetcher,
                  handleOnPrettifyClick: this.handleOnPrettifyClick,
                  handleOnResetClick: this.handleOnResetClick,
                  onEditQuery: this.handleOnEditQuery,
                  onEditVariables: this.handleOnEditVariables,
                  toggleInfoModel: this.toggleInfoModel,
                  toggleSaveModel: this.toggleSaveModel,
                  ...requestTabs[active]
                }
              },
              path:
                id === 'history'
                  ? `/request/history/${requestTabs[id].id}`
                  : `/request/collection/${requestTabs[id]
                      .collection}/${requestTabs[id].id}`
            }
          };
        }, {})) ||
      {}
    );
  }

  handleOnSidebarButtonClick(active) {
    const { setUiRequest } = this.props;

    setUiRequest({
      sidebarRequestContent: active
    });
  }

  toggleInfoModel() {
    const {
      activeRequestTabOrder,
      requestTabs,
      setUiRequest,
      uiRequest: { isInfoModalOpen }
    } = this.props;

    const tab = requestTabs[activeRequestTabOrder[0]];

    if (tab.query.trim() === '') {
      setUiRequest({ isInfoModalOpen: !isInfoModalOpen });
    }
  }

  toggleSaveModel() {
    const {
      activeRequestTabOrder,
      requestTabs,
      setSaveFormFields,
      setUiRequest,
      uiRequest: { isSaveModalOpen }
    } = this.props;

    const tab = requestTabs[activeRequestTabOrder[0]];

    if (isSaveModalOpen) {
      setSaveFormFields({
        name: { value: '' },
        collection: { value: '' },
        description: { value: '' }
      });
    }

    if (tab.query.trim() !== '') {
      const isHistory = tab.isHistory;

      setSaveFormFields({
        name: { value: isHistory ? '' : tab.name },
        collection: { value: isHistory ? '' : tab.collection },
        description: { value: isHistory ? '' : tab.description }
      });

      setUiRequest({ isSaveModalOpen: !isSaveModalOpen });
    }
  }

  validateSaveModule(data) {
    const errors = {};
    const { name, collection } = data;

    if (collection == null || collection.trim() === '') {
      errors.collection = 'Please enter a collection name';
    }

    if (name == null || name.trim() === '') {
      errors.name = 'Please enter a query name';
    }
    return Object.keys(errors).length !== 0 ? errors : null;
  }

  render() {
    const { component, uiRequest: { gqlTheme, gqlThemePaper } } = this.props;
    const Component = component;

    return (
      <Container
        className={`Request-container graphiql-theme ${gqlTheme} ${gqlThemePaper
          ? 'paper'
          : ''}`}
      >
        <Component {...getClassMethods(this)} />
      </Container>
    );
  }
}

export default connect(RequestContainer);
