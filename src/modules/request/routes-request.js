import React from 'react';
import Request from './components/Request';
import RequestContainer from './containers/container-request';

import { ROUTE_REQUEST } from '../router/route-constants';

export default {
  [ROUTE_REQUEST]: {
    component: props => <RequestContainer component={Request} />
  }
};
