import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import Accordion from '../../../styled/components/accordion/Accordion';
import AccordionButton from '../../../styled/components/accordion/Accordion-button';
import AccordionContent from '../../../styled/components/accordion/Accordion-content';
import AccordionItem from '../../../styled/components/accordion/Accordion-item';
import ContentLayout from '../../shared/components/Content-layout-shared';
import InfoModal from '../../shared/components/Info-modal-shared';
import List from '../../../styled/components/list/List';
import ListItem from '../../../styled/components/list/ListItem';
import ListItemNavLink from '../../../styled/components/list/ListItemNavLink';
import SaveModal from '../../shared/components/Save-modal-shared';
import Svg from '../../../styled/components/Svg';
import Tabs from '../../../styled/components/tabs/Tabs';
import historyIcon from '../../../icons/back-in-time.svg';
import moment from 'moment';
import queryIcon from '../../../icons/text-document.svg';

const Main = styled.div`
  display: flex;
  flex: 1;
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Label = styled.div``;
const Meta = styled.div``;

const HistoryInfo = props => (
  <div>
    <p> Use the query editor to left to send queries to the server. </p>
    <p>
      Queries typically start with a {`"{"`} character. Lines that start with a#
      are ignored.
    </p>
    <p> An example query might look like: </p>
    <pre>
      {' '}
      {`}
  User {
    id
    firstName
    lastName
  }
}`}
    </pre>

    <p>
      Keyboard shortcuts: <br />
      <br />
      Run Query: Ctrl - Enter or press the play button above the query editor.{' '}
      <br />
      <br />
      Auto Complete: Ctrl - Space or just start typing.
    </p>
  </div>
);

const CollectionInfo = props => <p>Save queries to create collections</p>;

export default class Request extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);

    this.state = {
      accordion: {}
    };
  }
  componentWillMount() {
    const {
      location: { pathname },
      setUiRequest,
      uiRequest: { route }
    } = this.props;

    if (route.trim() === '') {
      setUiRequest({ route: pathname });
    }
  }

  createCollectionRoutes(collections) {
    return Object.keys(collections).reduce((previous, key) => {
      const collection = collections[key];

      return [
        ...previous,
        ...Object.keys(collection).map(id => {
          return {
            path: `/request/collection/${key}/${id}`,
            label: collection[id].name,
            type: 'collection',
            id,
            collection: key
          };
        })
      ];
    }, []);
  }

  createHistoryRoutes(history) {
    return [
      ...Object.keys(history)
        .reverse()
        .map(key => {
          const date = new Date(parseInt(key, 10)).toISOString();

          return {
            path: `/request/history/${key}`,
            label: moment(date).format('L-HH:mm:ss'),
            type: 'history',
            id: key
          };
        })
    ];
  }

  getCollectionSidebarItems() {
    const { activeRequestTabOrder, requestCollection } = this.props;
    const active = activeRequestTabOrder[0];
    const routes = this.createCollectionRoutes(requestCollection);

    return Object.keys(requestCollection).map(key => {
      const items = routes
        .filter(route => route.collection === key)
        .map(item => {
          return this.getListItems({
            active,
            id: item.id,
            item,
            onClick: this.handleRequestCollectionItemClick
          });
        });

      return (
        <AccordionItem className="Collection-accordion-item" key={key}>
          <AccordionButton
            className="Collection-accordion-button"
            data-collection={key}
            kitid={key}
            onClick={this.handleOnAccordionClicked}
            opened={this.state.accordion[key]}
          >
            {key} {this.state.accordion[key]}
          </AccordionButton>
          <AccordionContent
            className="Collection-accordion-content"
            opened={this.state.accordion[key]}
          >
            {items}
          </AccordionContent>
        </AccordionItem>
      );
    });
  }

  getHistorySidebarItems() {
    const {
      activeRequestTabOrder,

      requestHistory,
      requestTabs
    } = this.props;

    const active =
      activeRequestTabOrder[0] === 'history'
        ? requestTabs.history.id
        : activeRequestTabOrder[0];
    const routes = this.createHistoryRoutes(requestHistory);

    return routes.filter(route => route.label).map(item => {
      const pathSplit = item.path.split('/');
      const id = pathSplit[pathSplit.length - 1];
      const query = requestHistory[id].query;

      return this.getListItems({
        active,
        onClick: this.handleOnHistoryItemClick,
        id,
        item,
        query
      });
    });
  }

  getListItems({ active, id, item, query, onClick }) {
    return (
      <ListItem key={item.path}>
        <ListItemNavLink
          to={item.path}
          data-route={item.path}
          data-active={id === active}
          onClick={onClick}
          data-type={item.type}
          data-collection={item.collection}
        >
          {item.icon && (
            <Svg small styledSvg="margin-right: 5px" src={item.icon} />
          )}
          <Label>{item.label}</Label>
          {query && <Meta>{`${query.substr(0, 28)}...`}</Meta>}
        </ListItemNavLink>
      </ListItem>
    );
  }

  handleRequestCollectionItemClick(event) {
    const { handleRequestCollectionItemClick } = this.props;

    handleRequestCollectionItemClick({
      ...event.currentTarget.dataset
    });
  }

  handleOnHistoryItemClick(event) {
    const { handleOnHistoryItemClick } = this.props;

    handleOnHistoryItemClick({ ...event.currentTarget.dataset });
  }

  handleOnAccordionClicked(event) {
    const collection =
      event.target.dataset.collection || event.currentTarget.dataset.collection;
    const bool = !this.state.accordion[collection] || false;

    this.setState({
      accordion: {
        ...this.state.accordion,
        [collection]: bool
      }
    });
  }

  handleOnOptionChange(event) {
    const { forms: { saveForm }, toggleSaveModel } = this.props;

    toggleSaveModel();
  }

  handleOnSaveClick(event) {
    const {
      handleOnSaveClick,
      forms: { saveForm },
      toggleSaveModel
    } = this.props;
    const { collection, description, name } = saveForm.fields;

    handleOnSaveClick({
      collection: collection.value,
      description: description.value,
      name: name.value
    });

    // reset save form
    toggleSaveModel();
  }

  collectionLabels() {
    return Object.keys(this.props.requestCollection).map(key => ({
      label: key,
      value: key
    }));
  }

  render() {
    const {
      activeRequestTabOrder,
      forms,
      handleOnCloseClick,
      handleOnSidebarButtonClick,
      handleOnTabChange,
      project,
      requestCollection,
      requestTabs,
      setSaveFormFields,
      tabItems,
      toggleInfoModel,
      toggleSaveModel,
      uiRequest: {
        isInfoModalOpen,
        isSaveModalOpen,
        route,
        sidebarRequestContent
      },
      validation
    } = this.props;

    const active = activeRequestTabOrder[0];
    const requestTab = (requestTabs && requestTabs[active]) || {};

    return (
      <ContentLayout
        className="Request"
        styledContentLayout="margin-top: 10px"
        active={route}
        sidebar={{
          styledSidebarContent: 'margin-top: 10px',
          active: sidebarRequestContent,
          buttonLeft: {
            component: (
              <Accordion className="Collection-accordion">
                {this.getCollectionSidebarItems(requestCollection)}
              </Accordion>
            ),
            icon: queryIcon,
            onClick: handleOnSidebarButtonClick,
            title: 'Operations',
            type: 'left'
          },
          buttonRight: {
            component: (
              <List styledList="padding-top: 0;">
                {this.getHistorySidebarItems()}
              </List>
            ),
            icon: historyIcon,
            onClick: handleOnSidebarButtonClick,
            title: 'History',
            type: 'right'
          }
        }}
      >
        <Main className="Request-main">
          <Content className="Request-content">
            <Tabs
              className="Request-tabs"
              items={tabItems()}
              active={active}
              onChange={handleOnTabChange}
              handleOnCloseClick={handleOnCloseClick}
            />
          </Content>

          <InfoModal
            endpoint={requestTab.endpoint}
            id={requestTab.id}
            projectId={project.id}
            infoModalHeader="Request Info"
            isHistory={requestTab.isHistory}
            name={requestTab.name}
            opened={isInfoModalOpen}
            results={requestTab.results}
            setInfoModal={toggleInfoModel}
          />

          <SaveModal
            collection={requestCollection}
            collectionLabels={this.collectionLabels()}
            forms={forms}
            onOptionChange={this.handleOnOptionChange}
            onSaveClick={this.handleOnSaveClick}
            opened={isSaveModalOpen}
            saveModalHeader="Save Request"
            setSaveFormFields={setSaveFormFields}
            setSaveModel={toggleSaveModel}
            validation={validation}
          />
        </Main>
      </ContentLayout>
    );
  }
}
