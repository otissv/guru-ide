import React, { PureComponent } from 'react';
import GraphiQL from 'graphiql';
import styled from 'styled-components';
import 'graphiql/graphiql.css';
import '../css/graphiql-guru.css';
import IconButton from '../../../styled/components/IconButton';
import prettifyIcon from '../../../icons/flash.svg';
import saveIcon from '../../../icons/save.svg';
import infoIcon from '../../../icons/info.svg';

const Container = styled.div`
  position: absolute;
  bottom: ${props => props.theme.spacing.small};
  left: 0;
  right: 0;
  top: 41px;
`;

export default class EditorRequest extends PureComponent {
  render() {
    const {
      editorTheme,
      handleOnPrettifyClick,
      toggleInfoModel,
      toggleSaveModel,
      query,
      results,
      schema,
      variables
    } = this.props;

    const response =
      (results &&
        results.response &&
        JSON.stringify(results.response.data, null, 2)) ||
      '';

    return (
      <Container className="Editor-request-container">
        <GraphiQL
          className="Editor-request-graphiql"
          {...this.props}
          editorTheme={editorTheme}
          query={query || ''}
          variables={variables || ''}
          response={response}
          schema={schema}
        >
          <GraphiQL.Toolbar
            className="Editor-request-graphiql-toolbar"
            id="graphiql-query-editor"
          >
            <IconButton
              className="Editor-request-graphiql-button"
              onClick={handleOnPrettifyClick}
              src={prettifyIcon}
              title="Prettify"
            />
            <IconButton
              className="Editor-request-graphiql-button"
              onClick={toggleInfoModel}
              src={infoIcon}
              title="Info"
            />
            <IconButton
              className="Editor-request-graphiql-button"
              onClick={toggleSaveModel}
              src={saveIcon}
              title="Save"
            />
          </GraphiQL.Toolbar>
        </GraphiQL>
      </Container>
    );
  }
}

{
  /* <IconButton
              className="Editor-request-graphiql-button"
              onClick={handleOnResetClick}
              src={refreshIcon}
              title="Refresh"
            /> */
}
