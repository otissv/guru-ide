import React from 'react';
import Persisted from './components/Persisted';
import PersistedContainer from './containers/container-persisted';
import { ROUTE_PERSISTED } from '../router/route-constants';

export default {
  [ROUTE_PERSISTED]: {
    component: props => <PersistedContainer {...props} component={Persisted} />
  }
};
