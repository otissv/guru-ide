import React, { PureComponent } from 'react';
import styled from 'styled-components';
import Toolbar from '../../../styled/components/toolbar/Toolbar';
import ToolbarItem from '../../../styled/components/toolbar/Toolbar-item';
import EditorRequest from '../../shared/components/editor/Editor-request-shared';
import EditorResult from '../../shared/components/editor/Editor-result-shared';
import IconButton from '../../../styled/components/IconButton';
import runIcon from '../../../icons/controller-play.svg';
import prettifyIcon from '../../../icons/flash.svg';
import saveIcon from '../../../icons/save.svg';
import infoIcon from '../../../icons/info.svg';
import Variables from './Editor-variables';

import InfoModal from '../../shared/components/Info-modal-shared';
import SaveModal from '../../shared/components/Save-modal-shared';

const Container = styled.div`
  position: absolute;
  bottom: ${props => props.theme.spacing.large};
  left: 0;
  right: 0;
  top: 110px;
  overflow: hidden;
  display: flex;
`;

const Result = styled.div`
  flex: 1;
  border-left: ${props => props.theme.border.thinSecondary};
  width: 50%;
`;

const Persisted = styled.div`
  flex: 1;
  width: 50%;
  display: flex;
  flex-direction: column;
`;

{
  /* 
  import Download from '../Download-shared';
  <Download data={results} fileName={active}>
          Results Download
        </Download> */
}

export default class PersistedEditor extends PureComponent {
  collectionLabels() {}

  render() {
    const {
      endpoint,
      fetcher,
      forms,
      handleOnPrettifyClick,
      id,
      isHistory,
      isInfoModalOpen,
      isSaveModalOpen,
      name,
      onEditQuery,
      onEditVariables,
      persistedCollection,
      projectId,
      query,
      results,
      setSaveFormFields,
      toggleInfoModel,
      toggleSaveModel,
      validation,
      variables
    } = this.props;

    const response =
      (results &&
        results.response &&
        Object.keys(results.response).length &&
        JSON.stringify(results.response, null, 2)) ||
      '';

    return (
      <div>
        <Toolbar styledToolbar="margin-top: 10px">
          <ToolbarItem className="Editor-toolbar-item">
            <IconButton
              title="Execute Persisted"
              onClick={fetcher}
              src={runIcon}
            />
          </ToolbarItem>

          <ToolbarItem className="Editor-toolbar-item">
            <IconButton
              title="Prettify Persisted"
              src={prettifyIcon}
              onClick={handleOnPrettifyClick}
            />
          </ToolbarItem>

          <ToolbarItem className="Editor-toolbar-item">
            <IconButton
              title="Persisted Information"
              src={infoIcon}
              onClick={toggleInfoModel}
            />
          </ToolbarItem>

          <ToolbarItem className="Editor-toolbar-item">
            <IconButton
              title="Save Persisted"
              src={saveIcon}
              onClick={toggleSaveModel}
            />
          </ToolbarItem>
        </Toolbar>

        <Container className="Persisted-container">
          <Persisted className="Persisted-editor">
            <EditorRequest
              className="Persisted-editor-request"
              query={query}
              handleOnChange={onEditQuery}
            />
            <Variables
              className="Persisted-editor-variables"
              value={variables}
              handleOnChange={onEditVariables}
            />
          </Persisted>

          <Result className="Persisted-editor-results-container">
            <EditorResult
              className="Persisted-editor-results"
              results={response}
              handleOnChange={onEditQuery}
            />
          </Result>
          <InfoModal
            endpoint={endpoint}
            id={id}
            projectId={projectId}
            infoModalHeader="Persisted Info"
            isHistory={isHistory}
            name={name}
            opened={isInfoModalOpen}
            results={results}
            setInfoModal={toggleInfoModel}
          />
          <SaveModal
            collection={persistedCollection}
            collectionLabels={this.collectionLabels()}
            forms={forms}
            handleClickSave={this.handleOnSaveClick}
            opened={isSaveModalOpen}
            saveModalHeader="Save Request"
            setSaveFormFields={setSaveFormFields}
            setSaveModel={toggleSaveModel}
            validation={validation}
          />
        </Container>
      </div>
    );
  }
}

{
  /* <ToolbarItem className="Editor-toolbar-item">
            <IconButton
              title="Reset Persisted"
              src={refreshIcon}
              onClick={handleOnResetClick}
            >
              <i className="Settings-icon icon-arrows-ccw"></i>
            </IconButton>
          </ToolbarItem> */
}
