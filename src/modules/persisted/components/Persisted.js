import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import Accordion from '../../../styled/components/accordion/Accordion';
import AccordionButton from '../../../styled/components/accordion/Accordion-button';
import AccordionContent from '../../../styled/components/accordion/Accordion-content';
import AccordionItem from '../../../styled/components/accordion/Accordion-item';
import ContentLayout from '../../shared/components/Content-layout-shared';
import InfoModal from '../../shared/components/Info-modal-shared';
import List from '../../../styled/components/list/List';
import ListItem from '../../../styled/components/list/ListItem';
import ListItemNavLink from '../../../styled/components/list/ListItemNavLink';
import SaveModal from '../../shared/components/Save-modal-shared';
import Sidebar from '../../../styled/components/sidebar/Sidebar';
import Svg from '../../../styled/components/Svg';
import Tabs from '../../../styled/components/tabs/Tabs';
import archiveIcon from '../../../icons/archive.svg';
import fileDownload from 'js-file-download';
import historyIcon from '../../../icons/back-in-time.svg';
import moment from 'moment';
import mutationIcon from '../../../icons/new-message.svg';
import queryIcon from '../../../icons/text-document.svg';

const Container = styled.div`
  display: flex;
  margin-top: ${props => props.theme.spacing.small};
`;

const Main = styled.div`
  display: flex;
  flex: 1;
`;

const Title = styled.h2`
  margin: 0;
  margin-bottom: ${props => props.theme.spacing.large};
`;

const Content = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Label = styled.div``;
const Meta = styled.div``;

const HistoryInfo = props => (
  <div>
    <p>Use the query editor to left to send persisted queries to the server.</p>
    <p>Queries are list with</p>
    <p>An example query might look like:</p>
    <pre>
      {`[
  {
    "id": "cj5uyf8ni",
    "variables": {
      "_id": "1"
    }
  }
]`}
    </pre>

    <p>
      Keyboard shortcuts:<br />
      <br />
      Run Persisted: Ctrl-Enter or press the play button above the query editor.<br
      />
      <br />
      Auto Complete: Ctrl-Space or just start typing.`}
    </p>
  </div>
);

const CollectionInfo = props => (
  <div>Save persisted queries to create collections</div>
);

export default class Persisted extends PureComponent {
  constructor(props) {
    super(...arguments);
    autobind(this);

    this.state = {
      accordion: {}
    };
  }
  componentWillMount() {
    const {
      location: { pathname },
      setUiPersisted,
      uiPersisted: { route }
    } = this.props;
    const pathSplit = pathname.split('/');
    const id = pathSplit[pathSplit.length - 1];

    if (route.trim() === '') {
      setUiPersisted({ route: pathname });
    }
  }

  createCollectionRoutes(collections) {
    return Object.keys(collections).reduce((previous, key) => {
      const collection = collections[key];

      return [
        ...previous,
        ...Object.keys(collection).map(id => {
          return {
            path: `/persisted/collection/${key}/${id}`,
            label: collection[id].name,
            type: 'collection',
            id,
            collection: key
          };
        })
      ];
    }, []);
  }

  createHistoryRoutes(history) {
    return [
      ...Object.keys(history)
        .reverse()
        .map(key => {
          const date = new Date(parseInt(key, 10)).toISOString();

          return {
            path: `/persisted/history/${key}`,
            label: moment(date).format('L-HH:mm:ss'),
            type: 'history',
            id: key
          };
        })
    ];
  }

  getCollectionSidebarItems() {
    const { activePersistedTabOrder, persistedCollection } = this.props;
    const active = activePersistedTabOrder[0];
    const routes = this.createCollectionRoutes(persistedCollection);

    return Object.keys(persistedCollection).map(key => {
      const items = routes
        .filter(route => route.collection === key)
        .map(item => {
          return this.getListItems({
            active,
            id: item.id,
            item,
            onClick: this.handlePersistedCollectionItemClick
          });
        });

      return (
        <AccordionItem className="Collection-accordion-item" key={key}>
          <AccordionButton
            className="Collection-accordion-button"
            data-collection={key}
            kitid={key}
            onClick={this.handleOnAccordionClicked}
            opened={this.state.accordion[key]}
          >
            {key} {this.state.accordion[key]}
          </AccordionButton>
          <AccordionContent
            className="Collection-accordion-content"
            opened={this.state.accordion[key]}
          >
            {items}
          </AccordionContent>
        </AccordionItem>
      );
    });
  }

  getHistorySidebarItems() {
    const {
      activePersistedTabOrder,
      addPersistedTabs,
      persistedHistory,
      persistedTabs,
      location: { pathname },
      uiPersisted: { route }
    } = this.props;

    const active =
      activePersistedTabOrder[0] === 'history'
        ? persistedTabs.history.id
        : activePersistedTabOrder[0];
    const routes = this.createHistoryRoutes(persistedHistory);

    return routes.filter(route => route.label).map(item => {
      const pathSplit = item.path.split('/');
      const id = pathSplit[pathSplit.length - 1];
      const query = persistedHistory[id].query;

      return this.getListItems({
        active,
        onClick: this.handleOnHistoryItemClick,
        id,
        item,
        query
      });
    });
  }

  getListItems({ active, id, item, query, onClick }) {
    return (
      <ListItem key={item.path}>
        <ListItemNavLink
          to={item.path}
          data-route={item.path}
          data-active={id === active}
          onClick={onClick}
          data-type={item.type}
          data-collection={item.collection}
        >
          {item.icon && (
            <Svg small styledSvg="margin-right: 5px" src={item.icon} />
          )}
          <Label>{item.label}</Label>
          {query && <Meta>{`${query.substr(0, 28)}...`}</Meta>}
        </ListItemNavLink>
      </ListItem>
    );
  }

  handlePersistedCollectionItemClick(event) {
    const { handlePersistedCollectionItemClick } = this.props;

    handlePersistedCollectionItemClick({
      ...event.currentTarget.dataset
    });
  }

  handleOnHistoryItemClick(event) {
    const { handlePersistedHistoryItemClick } = this.props;

    handlePersistedHistoryItemClick({ ...event.currentTarget.dataset });
  }

  handleOnAccordionClicked(event) {
    const collection =
      event.target.dataset.collection || event.currentTarget.dataset.collection;
    const bool = !this.state.accordion[collection] || false;

    this.setState({
      accordion: {
        ...this.state.accordion,
        [collection]: bool
      }
    });
  }

  render() {
    const {
      activePersistedTabOrder,
      getSidebarType,
      handleOnCloseClick,
      handleOnHistoryRemoveClick,
      handleOnTabChange,
      handlePersistedCollectionItemClick,
      handlePersistedHistoryItemClick,
      persistedCollection,
      persistedHistory,
      persistedTabs,
      sidebarItems,
      tabItems,
      handleOnSidebarButtonClick,
      uiPersisted: {
        isInfoModalOpen,
        isSaveModalOpen,
        route,
        sidebarPersistedContent
      }
    } = this.props;

    const active = activePersistedTabOrder[0];
    const activeId = (persistedTabs && persistedTabs[active].id) || '';

    return (
      <ContentLayout
        styledContentLayout="margin-top: 10px"
        className="Persisted"
        active={route}
        sidebar={{
          styledSidebarContent: 'margin-top: 10px',
          active: sidebarPersistedContent,
          buttonLeft: {
            component: (
              <Accordion className="Collection-accordion">
                {this.getCollectionSidebarItems(persistedCollection)}
              </Accordion>
            ),
            icon: queryIcon,
            onClick: handleOnSidebarButtonClick,
            title: 'Operations',
            type: 'left'
          },
          buttonRight: {
            component: (
              <List styledList="padding-top: 0;">
                {this.getHistorySidebarItems()}
              </List>
            ),
            icon: historyIcon,
            onClick: handleOnSidebarButtonClick,
            title: 'History',
            type: 'right'
          }
        }}
      >
        <Main className="Persisted-main">
          <Content className="Persisted-content">
            <Tabs
              className="Persisted-tabs"
              items={tabItems()}
              active={active}
              onChange={handleOnTabChange}
              handleOnCloseClick={handleOnCloseClick}
            />
          </Content>
        </Main>
      </ContentLayout>
    );
  }
}

{
  /* <Container className="Persisted">
        <Main className="Persisted-main">
          <Content className="Persisted-content">
            <Tabs
              className="Persisted-tabs"
              items={tabItems()}
              active={active}
              onChange={handleOnTabChange}
              handleOnCloseClick={handleOnCloseClick}
              styledTabList="position: initial; margin: 0"
            />
          </Content>
        </Main>
      </Container> */
}

{
  /* <HistoryCollectionSidebar
            active={sidebarPersistedContent}
            className="Persisted-History=collection-sidebar"
            collectionInfo={
              <CollectionInfo className="Persisted-collection-info" />
            }
            collections={persistedCollection}
            handleOnCollectionItemClick={handlePersistedCollectionItemClick}
            handleOnHistoryItemClick={handlePersistedHistoryItemClick}
            handleOnHistoryRemoveClick={handleOnHistoryRemoveClick}
            history={persistedHistory}
            historyInfo={<HistoryInfo className="Persisted-history-info" />}
            handleOnSidebarButtonClick={handleOnSidebarButtonClick}
            type={getSidebarType()}
          /> */
}
