import React from 'react';
import axios from 'axios';
import autobind from 'class-autobind';
import { connect } from '../../../store';
import getClassMethods from '../../../helpers/get-class-methods';
import cuid from 'cuid';
import PersistedEditor from '../components/Editor-persisted';
import { TAB_RESET_STATE } from '../../shared/constants-shared';

class PersistedContainer extends React.PureComponent {
  constructor() {
    super(...arguments);
    autobind(this);

    this.request = {
      history: {
        id: '',
        query: '',
        variables: ''
      },
      collections: {}
    };
  }

  componentWillMount() {
    const {
      createPersistedCollections,
      createPersistedHistory,
      getPersisted
    } = this.props;

    // fetch queries and history from server
    getPersisted()
      .payload.then(response => {
        if (response.data.idePersistedFindAll) {
          createPersistedCollections(response.data.idePersistedFindAll);
        }

        if (response.data.idePersistedHistoryFindAll) {
          createPersistedHistory(response.data.idePersistedHistoryFindAll);
        }
      })
      .catch(error => console.error(error));
  }

  fetcher(proxy, event) {
    const {
      activePersistedTabOrder,
      addPersistedHistoryItem,
      addPersistedTabs,
      savePersistedHistory,
      project,
      persistedTabs
    } = this.props;

    const active = activePersistedTabOrder[0];

    let query;
    let variables;

    if (active === 'history') {
      query = this.request.history.query;
      variables = this.request.history.variables;
    } else {
      query = this.request.collections.query;
      variables = this.request.collections.variables;
    }

    if (query === '') {
      return Promise.resolve('Please provide a query.');
    } else {
      const endpoint = project.endpoint;

      // axios defaults
      axios.defaults.baseURL = endpoint;
      axios.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded';

      const axiosConfig = {
        url: endpoint,
        method: 'POST'
      };

      const startResponseTime = new Date().getTime();

      return axios({
        ...axiosConfig,
        data: {
          query,
          variables,
          persisted: true
        }
      })
        .then(response => {
          if (!response.data.__schema) {
            const date = Date.parse(new Date());
            const isHistory = active === 'history';
            const id = isHistory ? date : active;
            const name = persistedTabs[active].name;

            const data = {
              id,
              endpoint,
              name,
              query: query,

              results: {
                request: {
                  data: response.config.data,
                  headers: response.config.headers,
                  method: 'POST',
                  url: response.config.url,
                  xsrfCookieName: response.config.xsrfCookieName,
                  xsrfHeaderName: response.config.xsrfHeaderName
                },

                headers: response.headers,
                status: `${response.status} ${response.statusText}`,
                code: 200,
                time: `${new Date().getTime() - startResponseTime}`,
                response: response.data
              },
              variables: variables ? JSON.stringify(variables) : ''
            };

            addPersistedHistoryItem({
              [date]: data
            });

            savePersistedHistory({
              ...data,
              id: date,
              projectId: project.id
            });

            addPersistedTabs(data, isHistory);
            if (!isHistory) addPersistedTabs(data);

            return response.data.data;
          } else {
            return '';
          }
        })
        .catch(error => {
          console.error(error);
          if (error.response) {
            // Response status code 2xx
            // setQueryResultProps({
            //   response: error.response.data,
            //   status: `${error.response.status} failed`,
            //   code: parseInt(error.response.code, 10),
            //   headers: error.response.headers
            // });
          } else if (error.persisted) {
            // No response was received
            console.error(error.persisted);
            // setQueryResultProps({
            //   response: error.response.data,
            //   status: '502 failed...',
            //   code: 502,
            //   headers: {}
            // });
          } else {
            console.error(error);
            // setQueryResultProps({
            //   response: 'Connection failed',
            //   status: '500 failed',
            //   code: 500,
            //   headers: {}
            // });
          }
        });
    }
  }

  getGraphQLSchema(endpoint) {
    const {
      getGraphqlSchema,
      project,
      setGraphqlSchema,
      setSchemaIsConnected
    } = this.props;

    if (endpoint !== project.endpoint) {
      getGraphqlSchema(endpoint)
        .payload.then(response => {
          if (response.data && response.data.__schema) {
            setGraphqlSchema(response.data);
            setSchemaIsConnected(true);
          }
        })
        .catch(error => {
          console.error(error);
          setGraphqlSchema({});
          setSchemaIsConnected(false);
        });
    }
  }

  getQuery() {
    const { activePersistedTabOrder, persistedTabs } = this.props;

    const tab = persistedTabs[activePersistedTabOrder[0]];
    const query = this.request.query || tab.query;

    if (query.trim() === '') {
      return '';
    } else {
      return JSON.stringify(JSON.parse(query), null, 2);
    }
  }

  getSidebarType() {
    const { uiRequest: { sidebarRequestContent } } = this.props;
    return sidebarRequestContent === 'left' ? 'left' : 'right';
  }

  handleOnEditQuery(query) {
    this.setQuery(query);
  }

  handleOnEditVariables(variables) {
    this.request.variables = variables;
  }

  handleOnPrettifyClick(event) {
    const query = this.getQuery(this.request.query);
    this.setQuery(query);
  }

  setQuery(query) {
    const {
      activePersistedTabOrder,
      persistedTabs,
      addPersistedTabs
    } = this.props;
    const id = activePersistedTabOrder[0];
    const isHistory = id === 'history';

    const tab = {
      ...persistedTabs[id],
      query
    };

    if (isHistory) {
      this.request.history.query = query;
    } else {
      if (this.request.collections[tab.collection]) {
        this.request.collections[tab.collection][id].query = query;
      } else {
        this.request.collections[tab.collection] = {
          id: '',
          query: '',
          variables: ''
        };
      }
    }

    addPersistedTabs(tab, isHistory);
  }

  handleChangeCollection(selectObject) {
    this.request.collection = selectObject;
    this.forceUpdate();
  }

  handleChangeInputCollection(selectObject) {
    this.request.collection = selectObject;
    this.forceUpdate();
  }

  handleOnResetClick() {
    const {
      activePersistedTabOrder,
      addPersistedTabs,
      project,
      persistedTabs,
      resetForm,
      updatePersistedCollection
    } = this.props;
    const id = activePersistedTabOrder[0];

    this.request = {
      id: '',
      query: '',
      variables: ''
    };

    const isHistory = id === 'history' ? true : false;

    const tab = {
      ...persistedTabs[id],
      ...TAB_RESET_STATE,
      endpoint: project.endpoint,
      isHistory
    };

    resetForm('saveForm');

    if (!isHistory) {
      addPersistedTabs(tab, isHistory);
      updatePersistedCollection(tab);
    }
  }

  handleClickSave(values) {
    const {
      addPersistedCollection,
      createPersisted,
      resetForm,
      selectedPersisted,
      setSelectedPersisted,
      setUiPersisted
    } = this.props;

    const { name, description } = values;

    const data = {
      ...selectedPersisted,
      ...this.request,
      collection: this.request.collection.value,
      description,
      name,
      query: this.request.query,
      id: selectedPersisted.id || cuid(),
      results: JSON.stringify(selectedPersisted.results)
    };

    setSelectedPersisted(data);
    addPersistedCollection(data);
    createPersisted(data)
      .payload.then(response => {
        if (response.data.idePersistedCreate.RESULTS_.result === 'failed') {
        }
      })
      .catch(error => console.error(error));
    setUiPersisted({ isSaveModalOpen: false });
    resetForm('saveForm');
  }

  handlePersistedCollectionItemClick({ id, collection }) {
    const {
      requestCollection,
      setActivePersistedTab,
      addPersistedTabs
    } = this.props;

    let query;
    let variables;

    if (
      this.request.collections[collection] &&
      this.request.collections[collection][id]
    ) {
      query = this.request.collections[collection][id].query;
      variables = this.request.collections[collection][id].variables;
    } else {
      query = requestCollection[collection][id].query;
      variables = requestCollection[collection][id].variables;

      this.request.collections = {
        ...this.request.collections,
        [collection]: {
          ...this.request.collections[collection],
          [id]: {
            id,
            query,
            variables
          }
        }
      };
    }

    const tab = {
      ...requestCollection[collection][id],
      query,
      variables
    };

    setActivePersistedTab(id);
    addPersistedTabs(tab);
  }

  handlePersistedHistoryItemClick({ route }) {
    const {
      persistedHistory,
      setActivePersistedTab,
      addPersistedTabs,

      setUiPersisted
    } = this.props;

    const id = route.split('/persisted/history/')[1];

    setActivePersistedTab('history');
    addPersistedTabs(persistedHistory[id], 'history');
    setUiPersisted({ route });
  }

  tabItems() {
    const {
      activePersistedTabOrder,
      persistedTabs,
      uiPersisted: { gqlTheme, isInfoModalOpen, isSaveModalOpen }
    } = this.props;

    const active = activePersistedTabOrder[0];

    return (
      (persistedTabs &&
        Object.keys(persistedTabs).reduce((previous, id) => {
          return {
            ...previous,
            [id]: {
              handleOnCloseClick: () => {},
              id,
              name: id === 'history' ? 'history' : persistedTabs[id].name,
              component: {
                element: PersistedEditor,
                props: {
                  active,
                  editorTheme: gqlTheme,
                  fetcher: this.fetcher,
                  handleOnChangePersisted: this.handleOnChangePersisted,
                  handleOnPrettifyClick: this.handleOnPrettifyClick,
                  handleOnResetClick: this.handleOnResetClick,
                  handleOnVariableChange: this.handleOnVariableChange,

                  isInfoModalOpen,
                  isSaveModalOpen,
                  onEditQuery: this.handleOnEditQuery,
                  onEditVariables: this.handleOnEditVariables,
                  toggleInfoModel: this.toggleInfoModel,
                  toggleSaveModel: this.toggleSaveModel,
                  ...persistedTabs[id]
                }
              }
            }
          };
        }, {})) ||
      {}
    );
  }

  handleOnSidebarButtonClick(active) {
    const { setUiPersisted } = this.props;

    setUiPersisted({
      sidebarPersistedContent: active
    });
  }

  toggleInfoModel(bool) {
    const {
      activePersistedTabOrder,
      persistedTabs,
      setUiPersisted,
      uiPersisted: { isInfoModalOpen }
    } = this.props;

    const tab = persistedTabs[activePersistedTabOrder[0]];
    if (tab.query.trim() !== '')
      setUiPersisted({ isInfoModalOpen: !isInfoModalOpen });
  }

  toggleSaveModel(bool) {
    const {
      selectedPersisted,
      setPersistedResultProps,
      setUiPersisted,
      uiPersisted: { isSaveModalOpen }
    } = this.props;
    if (selectedPersisted.query.trim() === '') {
      setPersistedResultProps({
        response: 'Please provide a persisted query.'
      });
    } else {
      setUiPersisted({ isSaveModalOpen: isSaveModalOpen });
    }
  }

  validateSaveModule(data) {
    const errors = {};
    const { name } = data;

    if (
      this.request.collection.value == null ||
      this.request.collection.value.trim() === ''
    ) {
      errors.collection = 'Please enter a collection name';
    }

    if (name == null || name.trim() === '') {
      errors.name = 'Please enter a persisted name';
    }
    return Object.keys(errors).length !== 0 ? errors : null;
  }

  render() {
    const Component = this.props.component;

    return <Component {...getClassMethods(this)} />;
  }
}

export default connect(PersistedContainer);
