import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import styled from 'styled-components';
import Logo from '../../../styled/components/Logo';
import logo from '../../../images/guru.svg';
import routes from '../../router/routes';
import NewWindowButton from './New-window-button-nav';
import Nav from '../../../styled/components/nav/Nav';
import NavList from '../../../styled/components/nav/NavList';
import NavListItem from '../../../styled/components/nav/NavListItem';
import NavLink from '../../../styled/components/nav/NavLink';

const Container = styled.div`
  height: 100vh;
  width: 220px;
`;

const Main = styled.div`
  background: ${props => props.theme.colors.background};
  bottom: 0;
  display: flex;
  display: flex;
  flex-direction: column;
  padding-top: ${props => props.theme.spacing.medium};
  height: calc(100vh - 55px);
  width: 180px;
  margin-right: ${props => props.theme.spacing.medium};
`;

class Navigation extends PureComponent {
  constructor() {
    super(...arguments);
    autobind(this);

    this.routes = routes[0].routes;

    const route = this.routes.filter(r => r.active)[0];

    this.state = {
      active: route
        ? route.label.toLocaleLowerCase()
        : routes[0].label.toLocaleLowerCase()
    };
  }

  componentWillMount() {
    const { setUiNavProps } = this.props;
    const route = this.routes.filter(r => r.active)[0].path;

    setUiNavProps({ route: route });
  }

  handleNavListItemClick(event) {
    const { setApp, setUiNavProps } = this.props;
    const { nav, route } = event.currentTarget.dataset;

    this.toggleUiNavigation();
    setApp({ title: nav });
    setUiNavProps({ route: route });
  }

  toggleUiNavigation() {
    const { uiNavigation } = this.props;
    this.props.setUiNavProps({ show: !uiNavigation.show });
  }

  getNavItems() {
    const { uiNavigation } = this.props;

    return this.routes.map(route => {
      const label = route.label.toLocaleLowerCase();

      return (
        <NavListItem className="Navigation-list-item" key={route.path}>
          <NavLink
            className="Navigation-list-item-link"
            to={route.path}
            data-nav={label}
            data-route={route.path}
            onClick={this.handleNavListItemClick}
            data-active={route.path === uiNavigation.route || false}
          >
            {`${label[0].toLocaleUpperCase()}${label.substr(
              1,
              label.length - 1
            )}`}
          </NavLink>
        </NavListItem>
      );
    });
  }

  render() {
    const { uiNavigation } = this.props;

    return (
      <Container className="Navigation-container" show={uiNavigation.show}>
        <Logo className="logo" src={logo} alt="Guru" />

        <Main className="Navigation-main" show={uiNavigation.show}>
          <Nav className="Navigation">
            <NavList className="Navigation-list">{this.getNavItems()}</NavList>
          </Nav>
          <NewWindowButton className="New-window-button" />
        </Main>
      </Container>
    );
  }
}

export default Navigation;
