import React, { PureComponent } from 'react';
import autobind from 'class-autobind';
import Button from '../../../styled/components/Button';
import plusIcon from '../../../icons/plus.svg';
import Svg from '../../../styled/components/Svg';
import styled from 'styled-components';

const ButtonStyled = styled(Button)`
  line-height: 18px;
  padding: 6px 0 8px 15px;
  text-align: left;
`;

export default class NewWindowButton extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  handleOnClick() {
    window.open(window.location.origin);
  }

  render() {
    return (
      <ButtonStyled {...this.props} onClick={this.handleOnClick}>
        <Svg styledSvg="transform: translate(-6px, 3px);" src={plusIcon} />New
        Window
      </ButtonStyled>
    );
  }
}
